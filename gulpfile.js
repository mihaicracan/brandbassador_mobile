var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;
elixir.config.assetsDir = "app/";
elixir.config.registerWatcher("imagemin", "app/img/**/*");
elixir.config.registerWatcher("htmlmin", ["app/templates/*.html", "app/index.html"]);

require("./elixir-tasks");

elixir(function(mix) {
    mix
        .sass([
        "ionic.app.scss",
        ], "public/css/app.css")

        .scripts([
        '../lib/ionic/js/ionic.bundle.custom.js',
        '../lib/ionic/js/ionic.superslidebox.js',
        '../lib/ngCordova/dist/ng-cordova.js',

        'app.js',
        'services/http.js',
        'services/local.js',
        'services/ga.js',
        'services/strings.js',
        'services/error.js',
        'services/style.js',

        'controllers/app.js',
        'controllers/login.js',
        'controllers/register.js',
        'controllers/forgot.js',
        'controllers/reset.js',
        'controllers/dashboard.js',
        'controllers/levels.js',
        'controllers/ranking.js',
        'controllers/goals.js',
        'controllers/missions.js',
        'controllers/codes.js',
        'controllers/payouts.js',
        'controllers/profile.js',
        'controllers/terms.js',
        'controllers/blogs.js',
        'controllers/salesteam.js',
        'controllers/social.js',
        'controllers/companies.js',

        'controllers/admin.js',
        'controllers/adminDashboard.js',
        'controllers/adminBrandbassadors.js',
        'controllers/adminMissions.js',
        'controllers/adminGoals.js',
        'controllers/adminSettings.js',
        ], 'public/js/app.js')

        // .uglify('public/js/*.js', 'public/js')
        .htmlmin('app/**/*.html', 'public')
        .imagemin('app/img/**/*', 'public/img');
});