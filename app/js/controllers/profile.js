
angular.module('starter')

.controller('ProfileCtrl', function($scope, $rootScope, $stateParams, $ionicPopup, $window, $timeout, LocalService, HttpService, GAService) {

	// Init Server Data
	HttpService.init("getProfile", "profile", "logged_in");
	HttpService.init("getCountries", "countries");
	HttpService.init("getStates", "states");
	HttpService.init("getSocialNetworks", "networks", "logged_in");

	$scope.profileData = {};

	// Init city and school input type as text
	$scope.cityInputType = "text";
	$scope.schoolInputType = "text";

	// generate days
	$scope.days = [];
	for (var i = 1; i <= 31; i++) {
	  $scope.days.push(i);
	};

	// generate months
	$scope.months = $rootScope.strings.months;

	// generate years
	$scope.years = [];
	for (var i = 1960; i <= 2010; i++) {
	  $scope.years.push(i);
	};  

	LocalService.get("profile", function(profile){
		// use timeout to force view loading before running this code
		$timeout(function(){
			$scope.profileData = profile;
		});
	});

	// Country changed listener
	$scope.onCountryChange = function(data) {
	  var country = $scope.profileData.country.id;

	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.profileData.city = "";
	  $scope.profileData.school = "";
	  
	  // update cities and schools based on country
	  HttpService.getCities({
	    country : country
	  });

	  HttpService.getSchools({
	    country : country
	  });
	}

	$scope.onStateChange = function() {
	  
	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.profileData.city = "";
	  $scope.profileData.school = "";

	  HttpService.getCities({
	    country : $scope.profileData.country.id,
	    state : $scope.profileData.state.id
	  });
	}

	$scope.onCityChange = function() {
	  var country = $scope.profileData.country.id;
	  var city    = $scope.profileData.city;

	    $scope.schoolInputType = "select";
	    $scope.profileData.school = "";

	  if (city == "-1") {
	    $scope.cityInputType = "text";
	    $scope.profileData.city = "";
	  }

	  HttpService.getSchools({
	    country : country,
	    city : city
	  });
	}

	$scope.onSchoolChange = function() {
	  var school = $scope.profileData.school.name;

	  if (parseInt(school) == -1) {
	    $scope.schoolInputType = "text";
	    $scope.profileData.school = [];
	  }
	}

	$scope.updateProfile = function(){
		GAService.trackEvent("Profile", "Update", "clicked", null);

		$ionicPopup.confirm({
			title: $rootScope.strings.profile.confirmTitleUpdate,
			template: $rootScope.strings.profile.confirmUpdate
		}).then(function(confirm){
			if (confirm) {
				GAService.trackEvent("Profile", "Update", "confirmed", null);

				// init data
				var country = $scope.profileData.country.short_name;
				var id_country = $scope.profileData.country.id;
				var id_state = $scope.profileData.state.id;
				var school = $scope.profileData.school.name;
				var graduation_year = $scope.profileData.school.graduation_year;

				$scope.profileData.country = country;
				$scope.profileData.id_country = id_country;
				$scope.profileData.id_state = id_state;
				$scope.profileData.school = school;
				$scope.profileData.graduation_year = graduation_year;

				// send information to server
				HttpService.updateProfile($scope.profileData, function(data){
					GAService.trackEvent("Profile", "Update", "success", null);

					HttpService.getProfile(function(data){
						$scope.profileData = data;
						$scope.schoolInputType = "text";
						$scope.$apply();

						$ionicPopup.alert({
						  title: $rootScope.strings.profile.alertTitleUpdate,
						  template: $rootScope.strings.profile.alertUpdateSuccess
						}); 
					});
				}, function(data){
					HttpService.getProfile(function(data){
						$scope.profileData = data;
						$scope.$apply();
					});
				});
			}
		});
	}
});
