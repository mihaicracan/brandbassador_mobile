
angular.module('starter')

.controller('SocialCtrl', function($scope, $rootScope, $stateParams, $window, LocalService, HttpService, ErrorService, GAService, $ionicPopup, $location) {

	// Init Server Data
	HttpService.init("getSocialNetworks", "networks");

	$scope.FBProfile = {};
	$scope.FBPages   = [];
	$scope.Pinterest = {};
	$scope.Vine      = {};


	$scope.chooseAccountPopup = null;

	// link Social Network by hash
	if (angular.isDefined($stateParams.hash)) {
		HttpService.getSocialNetworks($stateParams.hash, function(response){
			$rootScope.networks = response.data.networks;
		});
	}

	// get Twitter user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "twitter" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		HttpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			oauth_token: $stateParams.param1,
			oauth_verifier: $stateParams.param2
		}, function(response){
			
			$scope.linkSocialAccount({
				network: "twitter",
				profile: "http://twitter.com/"+response.data.user.screen_name,
				followers: response.data.user.followers_count,
				app_id: response.data.user.id
			});
		
		});
	}

	// get Instagram user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "instagram" && angular.isDefined($stateParams.param1)) {

		HttpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			code: $stateParams.param1
		}, function(response){
			
			$scope.linkSocialAccount({
				network: "instagram",
				profile: response.data.user.profile,
				followers: response.data.user.followers,
				app_id: response.data.user.user_id
			});
		
		});
	}

	// get Tumblr user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "tumblr" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		HttpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			oauth_token: $stateParams.param1,
			oauth_verifier: $stateParams.param2
		}, function(response){
			
			$scope.linkSocialAccount({
				network: "tumblr",
				profile: response.data.user.profile,
				followers: response.data.user.followers
			});
		
		});
	}

	// get Youtube user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "youtube" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		HttpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			state: $stateParams.param1.replace("----", "/"),
			code: $stateParams.param2.replace("----", "/")
		}, function(response){
			
			$scope.linkSocialAccount({
				network: "youtube",
				profile: response.data.user.profile,
				followers: response.data.user.followers
			});
		
		});
	}

	// fb link click listener
	$scope.doFBLink = function(){
		GAService.trackEvent("Social", "Facebook", "clicked", null);

		FB.getLoginStatus(function(response){
		   	if (response.status != "connected") {
		    	FB.login(function(response){
		      		if (response.status == "connected") {
		        		$scope.getFBAccounts();
		      		} else {
		        		$ionicPopup.alert({
		          			title: $rootScope.strings.social.alertTitleFB,
		          			template: $rootScope.strings.social.alertLinkErrorFB
		        		});
		      		}
		    	}, {scope: 'public_profile, email, manage_pages'});
		  	} else {
		  		$scope.getFBAccounts();
		  	}
		});
	}

	$scope.doTWLink = function(){
		GAService.trackEvent("Social", "Twitter", "clicked", null);

		HttpService.authNetworkRequest({
			service: "twitter",
			step: "redirectUrl"
		}, function(response){
			$window.location.href = response.data.url;
		});
	}

	$scope.doINSTLink = function(){
		GAService.trackEvent("Social", "Instagram", "clicked", null);

		HttpService.authNetworkRequest({
			service: "instagram",
			step: "redirectUrl"
		}, function(response){
			$window.location.href = response.data.url;
		});
	}

	$scope.doTUBLink = function(){
		GAService.trackEvent("Social", "Tumblr", "clicked", null);

		HttpService.authNetworkRequest({
			service: "tumblr",
			step: "redirectUrl"
		}, function(response){
			$window.location.href = response.data.url;
		});
	}

	$scope.doPINLink = function(){
		GAService.trackEvent("Social", "Pinterest", "clicked", null);

		$ionicPopup.show({
		    template: '<input type="text" ng-model="Pinterest.username">',
		    title: $rootScope.strings.social.popupTitlePIN,
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Cancel' },
		      	{	
		        	text: '<b>Send</b>',
		        	type: 'button-positive',
		        	onTap: function(e) {
		          		if (angular.isUndefined($scope.Pinterest.username) || $scope.Pinterest.username.length == 0) {
		            		//don't allow the user to close unless he enters Pinterest username
		            		e.preventDefault();
		          		} else {
		            		HttpService.authNetworkRequest({
		            			service: "pinterest",
		            			step: "getUser",
		            			username: $scope.Pinterest.username
		            		}, function(response){
		            			$scope.linkSocialAccount({
									network: "pinterest",
									profile: response.data.user.profile,
									followers: response.data.user.followers
								});
		            		});
		          		}
		        	}
		      	}
		    ]
		})
	}

	$scope.doYBLink = function(){
		GAService.trackEvent("Social", "Youtube", "clicked", null);

		HttpService.authNetworkRequest({
			service: "youtube",
			step: "redirectUrl"
		}, function(response){
			$window.location.href = response.data.url;
		});
	}

	$scope.doVNLink = function(){
		GAService.trackEvent("Social", "Vine", "clicked", null);

		$ionicPopup.show({
		    template: '<input type="text" ng-model="Vine.userID">',
		    title: $rootScope.strings.social.popupTitleVN,
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Cancel' },
		      	{	
		        	text: '<b>Send</b>',
		        	type: 'button-positive',
		        	onTap: function(e) {
		          		if (angular.isUndefined($scope.Vine.userID) || $scope.Vine.userID.length == 0) {
		            		//don't allow the user to close unless he enters Vine user ID
		            		e.preventDefault();
		          		} else {
		            		HttpService.authNetworkRequest({
		            			service: "vine",
		            			step: "getUser",
		            			userID: $scope.Vine.userID
		            		}, function(response){
		            			$scope.linkSocialAccount({
									network: "vine",
									profile: response.data.user.profile,
									followers: response.data.user.followers
								});
		            		});
		          		}
		        	}
		      	}
		    ]
		})
	}

	$scope.getFBAccounts = function(){
		var pages = [];
		FB.api("/me", function(response){
			$scope.FBProfile = response;

			FB.api("/me/accounts", function(response){
				if (angular.isDefined(response.data)) {
					$scope.FBPages = response.data
				}

				$scope.chooseFBAccount();
			});
		});
	}

	$scope.chooseFBAccount = function(){
		$scope.chooseAccountPopup = $ionicPopup.show({
			title: $rootScope.strings.social.alertTitleChooseFBAccount,
			scope: $scope,
			buttons: [
				{text: "Cancel"}
			],
			template: 	"<ion-list> \
					  		<ion-item ng-click='linkFBAccount()'> \
					    		{{FBProfile.first_name}} {{FBProfile.last_name}} \
					  		</ion-item> \
					  		<ion-item ng-repeat='page in FBPages' ng-click='linkFBPage(page.id)'> \
					    		{{page.name}} \
					  		</ion-item> \
						</ion-list>"
		});
	}

	$scope.linkSocialAccount = function(accountData){
		HttpService.linkSocialNetwork(accountData, function(data){

			GAService.trackEvent("Social", accountData.network.capitalize(), "linked", null);

			HttpService.getSocialNetworks();

			if (accountData.network == "facebook") {
				$scope.chooseAccountPopup.close();
			}

		});
	}

	$scope.linkFBAccount = function(){
		FB.api("/me/friends", function(response){

			if (angular.isDefined(response.summary) && angular.isDefined(response.summary.total_count)) {
				$scope.FBProfile.friends = response.summary.total_count;

				$scope.linkSocialAccount({
					network: "facebook",
					profile: $scope.FBProfile.link,
					followers: $scope.FBProfile.friends,
					app_id: $scope.FBProfile.id
				});
			}			
		});
	}

	$scope.linkFBPage = function(pageID){
		FB.api("/"+pageID, function(response){

			$scope.linkSocialAccount({
				network: "facebook",
				profile: response.link,
				followers: response.likes
			});
		});
	}

	$scope.completeRegistration = function(){
		HttpService.completeRegistration(function(data){
			$location.path("/login");

			GAService.trackEvent("Register", "PieceKeepers", "completed", null);

    		$ionicPopup.alert({
	      		title: $rootScope.strings.social.alertTitleRegisterSuccess,
	      		template: $rootScope.strings.social.alertRegisterSuccess
    		});
		});
	}

	$scope.showNetworkFollowers = function(network){
		if (angular.isDefined($rootScope.networks) && network in $rootScope.networks) {
			return true;
		}

		return false;
	}
});
