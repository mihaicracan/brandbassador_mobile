
angular.module('starter')

.controller('RankingCtrl', function($scope, $rootScope, $stateParams, $window, LocalService, HttpService, GAService) {

	// Init Server Data
	HttpService.init("getGlobalRanking", "globalRanking", "logged_in");
	HttpService.init("getProfile", "profile", "logged_in");

	// init layout details
	LocalService.get("profile", function(profile){
		var types = ["global", "country", "city"];

		for (var i in types) {
			if (profile.ranking[types[i]].last_month > 0) {
				$rootScope.profile.ranking[types[i]].arrow = "success ion-arrow-up-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelLastMonth;
			} else if (profile.ranking[types[i]].last_month == 0) {
				$rootScope.profile.ranking[types[i]].arrow = "success ion-arrow-up-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelSameLastMonth;
			} else {
				$rootScope.profile.ranking[types[i]].arrow = "error ion-arrow-down-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelLastMonth;
			}
		}
	});

});
