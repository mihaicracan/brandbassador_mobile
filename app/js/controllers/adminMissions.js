
angular.module('starter')

// directive for settings accordion reposition after toggle goal
.directive("mimssionDetailsRepeat", function($ionicScrollDelegate, $timeout){
	return function(scope, elements, attrs){
		scope.$watch(function(){
			return elements;
		}, function(elements){

			for (var i = elements.length - 1; i >= 0; i--) {
				var el = elements[i]

				var value = el.lastElementChild.className;
				if (value == "item-accordion item-text-wrap item") {
					$ionicScrollDelegate.resize();
					$ionicScrollDelegate.scrollTop(true);
				}

				var item_accordion = el.childNodes[3];

				angular.element(item_accordion).bind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function(event){

				});
			};

		});
	}
})

.controller('AdminMissionsCtrl', function($scope, $rootScope, $ionicModal, $stateParams, HttpService, $location, $timeout, $ionicPopup, $window, $ionicScrollDelegate, GAService) {
	HttpService.init("getAdminMissions", "adminMissions", "logged_in");

	$scope.missionsNextPage = 2;
	$scope.usersNextPage    = 2;
	$scope.noMoreMissions = false;
	$scope.noMoreUser     = false;
	$scope.idMission      = null;
	$scope.missionUserApplication = {};
	$scope.applications   = [];

	$rootScope.missionDetails = angular.copy($rootScope.currentMission);
	$scope.current_active = 0;
	$scope.items = [{
		title : $rootScope.strings.admin.missionEdit.labelGeneral,
		inputs : [{
			label : $rootScope.strings.admin.missionEdit.inputInfoFeatured,
			value : "featured",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoCountdown,
			value : "countdown",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoName,
			value : "name",
			type  : "text"
		}]
	}, {
		title : $rootScope.strings.admin.missionEdit.labelResponse,
		inputs : [{
			label : $rootScope.strings.admin.missionEdit.inputInfoText,
			value : "allow_text",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoLink,
			value : "allow_link",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoImage,
			value : "allow_image",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoLinkDetails,
			value : "link_details",
			type  : "text"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoTextDetails,
			value : "text_details",
			type  : "text"
		}]
	}, {
		title : $rootScope.strings.admin.missionEdit.labelFacebookShare,
		inputs : [{
			label : $rootScope.strings.admin.missionEdit.inputInfoFBShare,
			value : "fb_share",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoFBLink,
			value : "fb_link",
			type  : "text"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoFBText,
			value : "fb_text",
			type  : "text"
		}]
	}, {
		title : $rootScope.strings.admin.missionEdit.labelTwitterShare,
		inputs : [{
			label : $rootScope.strings.admin.missionEdit.inputInfoTWShare,
			value : "tw_share",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoTWLink,
			value : "tw_link",
			type  : "text"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoTWText,
			value : "tw_text",
			type  : "text"
		}]
	}, {
		title : $rootScope.strings.admin.missionEdit.labelPinterestShare,
		inputs : [{
			label : $rootScope.strings.admin.missionEdit.inputInfoPINShare,
			value : "pin_share",
			type  : "checkbox"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoPINLink,
			value : "pin_link",
			type  : "text"
		}, {
			label : $rootScope.strings.admin.missionEdit.inputInfoPINText,
			value : "pin_text",
			type  : "text"
		}]
	}];

	// generate days
	$scope.days = [];
	for (var i = 1; i <= 31; i++) {
	  $scope.days.push(i);
	};

	// generate months
	$scope.months = $rootScope.strings.months;

	// generate years
	$scope.years = [];
	for (var i = 1960; i <= 2010; i++) {
	  $scope.years.push(i);
	}; 

	for (var i = $scope.items.length - 1; i >= 0; i--) {
		$scope.items[i].arrow_type = "ion-arrow-right-b";
	};

	$scope.shownItem = $scope.items[0];
	$scope.items[0].arrow_type = "ion-arrow-down-b";

	$scope.hasMoreMissions = function(){
		return $scope.noMoreMissions ? false : true;
	}

	$scope.hasMoreUsers = function(){
		return $scope.noMoreUsers ? false : true;
	}

	$scope.loadMoreMissions = function(){

		HttpService.getAdminMissions({
			limit : 20,
			page  : $scope.missionsNextPage 
		}, function(response){
			if (response.data.missions.length > 0) {
				$scope.missionsNextPage++;
			} else {
				$scope.noMoreMissions = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.loadMoreUsers = function(){

		HttpService.getAdminMissionUsers({
			id_mission : $stateParams.idMission,
			limit : 20,
			page  : $scope.usersNextPage 
		}, function(response){
			if (response.data.participants.length > 0) {
				$scope.usersNextPage++;
			} else {
				$scope.noMoreUsers = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.viewMission = function(mission){
		GAService.trackEvent("Admin", "Mission", "view-clicked", null);

		mission.id_mission  = mission.id;
		$rootScope.currentMission = mission;
	}

	$scope.viewApplication = function(user){
		GAService.trackEvent("Admin", "Mission", "view-application-clicked", null);

		$rootScope.currentMissionUser = user;
	}

	$scope.onApproveClick = function(){
		GAService.trackEvent("Admin", "Mission", "approve-application-clicked", null);

		var idUser    = $stateParams.idUser;
		var idMission = $stateParams.idMission;

		$ionicPopup.confirm({
		  title: $rootScope.strings.admin.missionUserApplication.confirmTitleApprove,
		  template: $rootScope.strings.admin.missionUserApplication.confirmApprove
		}).then(function(confirm){
			if (confirm) {
				GAService.trackEvent("Admin", "Mission", "approve-application-confirmed", null);

				// approve mission on server
				HttpService.approveAdminMission({
					idUser : idUser,
					idMission : idMission
				}, function(response){
					GAService.trackEvent("Admin", "Mission", "approve-application-success", null);

					$ionicPopup.alert({
					  title: $rootScope.strings.admin.missionUserApplication.alertTitleApproveCompleted,
					  template: $rootScope.strings.admin.missionUserApplication.alertApproveCompleted 
					});  

					HttpService.getAdminMissionUserApplication({
						id_mission : $stateParams.idMission,
						id_user : $stateParams.idUser
					}, function(response){
						$scope.missionUserApplication = response.data.participant;
						$scope.missionUserApplication.submitted = $scope.missionUserApplication.submitted.toString() + "000"; 
					});
				});
			}
		});
	}

	$scope.onRejectClick = function(){
		GAService.trackEvent("Admin", "Mission", "reject-application-clicked", null);

		var idUser    = $stateParams.idUser;
		var idMission = $stateParams.idMission;

		$ionicPopup.show({
		    title: $rootScope.strings.admin.missionUserApplication.confirmTitleReject,
		    template: $rootScope.strings.admin.missionUserApplication.confirmReject +
		    			"<br><br><textarea ng-model='$parent.details'></textarea>",
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Cancel' },
		      	{	
		        	text: '<b>Reject</b>',
		        	type: 'button-assertive',
		        	onTap: function(e) {
		          		if (angular.isUndefined($scope.details) || $scope.details.length == 0) {
		            		//don't allow the user to close unless he enters Vine user ID
		            		e.preventDefault();
		          		} else {
		          			GAService.trackEvent("Admin", "Mission", "reject-application-confirmed", null);

		            		// reject mission on server
		            		HttpService.rejectAdminMission({
		            			idUser : idUser,
		            			idMission : idMission,
		            			details : $scope.details
		            		}, function(response){

		            			GAService.trackEvent("Admin", "Mission", "reject-application-success", null);

		            			$ionicPopup.alert({
		            			  title: $rootScope.strings.admin.missionUserApplication.alertTitleRejectCompleted,
		            			  template: $rootScope.strings.admin.missionUserApplication.alertRejectCompleted 
		            			});  

		            			HttpService.getAdminMissionUserApplication({
		            				id_mission : $stateParams.idMission,
		            				id_user : $stateParams.idUser
		            			}, function(response){
		            				$scope.missionUserApplication = response.data.participant;
		            				$scope.missionUserApplication.submitted = $scope.missionUserApplication.submitted.toString() + "000"; 
		            			});
		            		});
		          		}
		        	}
		      	}
		    ]
		})
	}

	$scope.editMission = function(idMission) {
		GAService.trackEvent("Admin", "Mission", "edit-clicked", null);

		$location.path("admin/missions/edit/"+idMission);
	}

	/*
	 * open an item
	 */
	$scope.toggleItem = function(index) {
		if (index == $scope.current_active) {
			return;
		}

		// get info tag height
		var info_tag = angular.element(document.getElementById("mission-info"));
		var offsetY = info_tag[0].offsetHeight;

		// get height of every item above current one
		for (i = 0; i < index; i++) {
			var el = angular.element(document.getElementById("item-"+i));
			offsetY += el[0].offsetHeight;
		}

		$ionicScrollDelegate.$getByHandle("mission").resize();
		$ionicScrollDelegate.$getByHandle("mission").scrollTo(0, offsetY, true);

	    $scope.shownItem = $scope.items[index];
	    $scope.items[index].arrow_type = "ion-arrow-down-b";
	    $scope.items[$scope.current_active].arrow_type = "ion-arrow-right-b";

	    $scope.current_active = index;
	};

	$scope.isItemShown = function(index) {
	   	return $scope.shownItem === $scope.items[index];
	};

	$scope.onSaveClick = function(){
		GAService.trackEvent("Admin", "Mission", "save-clicked", null);

		$ionicPopup.confirm({
		  title: $rootScope.strings.admin.missionEdit.confirmTitleEdit,
		  template: $rootScope.strings.admin.missionEdit.confirmEdit
		}).then(function(confirm){
			if (confirm) {
				GAService.trackEvent("Admin", "Mission", "save-confirmed", null);

				var missionDetails = angular.copy($rootScope.missionDetails);

				missionDetails.allow_image = missionDetails.allow_image ? 1 : 0;
				missionDetails.allow_link  = missionDetails.allow_link ? 1 : 0;
				missionDetails.allow_text  = missionDetails.allow_text ? 1 : 0;
				missionDetails.countdown   = missionDetails.countdown ? 1 : 0;
				missionDetails.deleted     = missionDetails.deleted ? 1 : 0;
				missionDetails.sales_goals = missionDetails.sales_goals ? 1 : 0;
				missionDetails.featured    = missionDetails.featured ? 1 : 0;
				missionDetails.fb_share    = missionDetails.fb_share ? 1 : 0;
				missionDetails.pin_share   = missionDetails.pin_share ? 1 : 0;
				missionDetails.tw_share    = missionDetails.tw_share ? 1 : 0;

				// send request to server
				HttpService.editMission(missionDetails, function(){
					GAService.trackEvent("Admin", "Mission", "save-success", null);

					HttpService.getAdminMissions({
						limit: 20,
						page: 1
					}, function(){
						$scope.missionsNextPage = 2;
					});
				});
			}
		});
	}

	if (angular.isDefined($stateParams.idMission) && angular.isDefined($stateParams.idUser)) {

		HttpService.getAdminMissionUserApplication({
			id_mission : $stateParams.idMission,
			id_user : $stateParams.idUser
		}, function(response){
			$scope.missionUserApplication = response.data.participant;
			$scope.missionUserApplication.submitted = $scope.missionUserApplication.submitted.toString() + "000"; 
		});
	}

	else if (angular.isDefined($stateParams.idMission)) {

		HttpService.getAdminMissionUsers({
			id_mission : $stateParams.idMission,
			limit : 20,
			page : 1
		}, function(response){
			$scope.applications["_"+$stateParams.idMission] = response.data.participants;
			$scope.idMission = $stateParams.idMission;
		});
	}
});
