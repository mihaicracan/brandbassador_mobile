angular.module('starter')

.controller('MissionsCtrl', function($scope, $rootScope, $window, $stateParams, $ionicPopup, LocalService, HttpService, GAService) {

	// Init Missions from Server
	HttpService.init("getMissions", "missions", "logged_in");
	
	// store user inputs for missions 
	$scope.missionsData = [];

	// store missions social sharing details 
	$scope.missionsShareData = [];

	// message for upload image button
	$scope.upload_message = [];

	// store images provided by user
	$scope.images = [];

	$scope.pin_shared  = false;
	$scope.pin_clicks  = 0;

	// init missions details
	$scope.getMissions = function(){
		LocalService.get("missions", function(missions){

			for (var i in missions) {
				// set initial image buttons label
				$scope.upload_message[missions[i].id] = $rootScope.strings.missions.labelUploadPhoto;

				// remove anchors from images for description
				$rootScope.missions[i].description = $rootScope.removeImageAnchors($rootScope.missions[i].description); 

				// init this object
				$scope.missionsShareData[missions[i].id] = {
					facebook : {
						link : missions[i].fb_link,
						text : missions[i].fb_text
					},
					twitter : {
						link : missions[i].tw_link,
						text : missions[i].tw_text
					},
					pinterest : {
						image : missions[i].pin_image,
						link  : missions[i].pin_link,
						text  : missions[i].pin_text
					}
				}

				// init missions status labels
				if (!missions[i].submitted) {
					missions[i].status = "incompleted";
				}

				if (missions[i].status == "incompleted" || missions[i].status == "participate") {
					$rootScope.missions[i].status_class = "incompleted";
					$rootScope.missions[i].status_text  = $rootScope.strings.missions.labelIncompleted;
				}
				else if (missions[i].status == "waiting") {
					$rootScope.missions[i].status_class = "waiting";
					$rootScope.missions[i].status_text  = $rootScope.strings.missions.labelWaiting;
				} else {
					$rootScope.missions[i].status_class = "completed";
					$rootScope.missions[i].status_text  = $rootScope.strings.missions.labelCompleted;
				}
			}
		});
	}
	$scope.getMissions();

	// Post a mission to server
	$scope.postMission = function(id_mission){

		GAService.trackEvent("Missions", "Post", "clicked", null);

		if (angular.isUndefined($scope.missionsData[id_mission])) {
			$scope.missionsData[id_mission] = {};
		}
		
		// send mission to server
		HttpService.postMission(id_mission, $scope.missionsData[id_mission], function(data){
			GAService.trackEvent("Missions", "Post", "success", null);

			$ionicPopup.alert({
			  title: $rootScope.strings.missions.alertTitleSubmitted,
			  template: $rootScope.strings.missions.alertSubmittedSuccess
			});

			// refresh missions details
			$scope.refreshMissions();

			if (angular.isDefined($scope.images[id_mission])) {
				HttpService.uploadMissionImage(id_mission, $scope.images[id_mission], function(data){
					
				});
			}

		});
	}


	$scope.shareFacebookMission = function(id_mission){
		GAService.trackEvent("Missions", "Facebook", "clicked", null);

		var data = $scope.missionsShareData[id_mission].facebook;

		// show sharing popup
		FB.ui({
           	method: 'feed',
           	link: data.link,
           	description: data.text

        }, function(response) {
        	// check if share is completed
           	if (response && response.post_id) {

           		GAService.trackEvent("Missions", "Facebook", "success", null);

           		// mark share completed on server
        		HttpService.postSharedMission(id_mission, "fb", function(data){
    				// display message popup
    				$ionicPopup.alert({
    				  title: $rootScope.strings.missions.alertTitleShared,
    				  template: $rootScope.strings.missions.alertSharedFBSuccess
    				});        				
    				// reload missions list
    				$scope.refreshMissions();
        		});
           	}
        });
	}

	$scope.listenTwitterEvent = function(){
		// init Twitter object
		$window.twttr = (function (d, s, id) {
		    var t, js, fjs = d.getElementsByTagName(s)[0];
		    if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
		    js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
		    return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
		}(document, "script", "twitter-wjs"));

		// use it when ready
		$window.twttr.ready(function (twttr) {
			// listen for tweet event
		  	twttr.events.bind('tweet', function (event) {
		  		var id_mission = event.target.attributes['data-id'].value;

		  		GAService.trackEvent("Missions", "Twitter", "clicked", null);

      	   		// mark share completed on server
      			HttpService.postSharedMission(id_mission, "tw", function(data){
  					// display message popup
  					$ionicPopup.alert({
  					  title: $rootScope.strings.missions.alertTitleShared,
  					  template: $rootScope.strings.missions.alertSharedTWSuccess
  					});        				
  					// reload missions list
  					$scope.refreshMissions();
      			});
		  	});
		});
	}

	$scope.listenTwitterEvent();

	// Reload all missions information
	$scope.refreshMissions = function(){
		HttpService.getMissions(function(data){
			$scope.getMissions();
		});

		HttpService.getProfile();
	}

	// Display Share button if not already shared
	$scope.showShareButton = function(share, shared){
		share  = parseInt(share)  ? 1 : 0;
		shared = parseInt(shared) ? 1 : 0;
		if (share == 1 && shared == 0) {
			return true;
		}

		return false;
	}

	// Missions image selector listener
	$scope.onFileSelected = function(file) {
		var selector = file.attributes["data-mission"].value; 
		var data = file.value.split("\\");
	    $scope.upload_message[selector] = data[data.length-1];

	    $scope.$apply();
	}

	// Pin share listener
	$scope.onPinShare = function(){
		GAService.trackEvent("Missions", "Pinterest", "clicked", null);
		$scope.pin_shared = true;
	}

	// Refresh missions information if Pin shared
	$scope.checkPinShare = function(){
		if ($scope.pin_shared) {
			$scope.pin_clicks++;
		}	

		if ($scope.pin_clicks > 5) {
			$scope.refreshMissions();
			$scope.pin_shared = false;
			$scope.pin_clicks = 0;
		}
	}

	// Display Mission send button
	$scope.showSendButton = function(mission){

		if (mission.is_post_mission || (!mission.is_sharing_mission && !mission.is_sale_mission)) {
			return true;
		}

		return false;
	}

	$scope.showDetails = function(status) {
		if (status == "incompleted" || status == "participate") {
			return true;
		}

		return false;
	}
});
