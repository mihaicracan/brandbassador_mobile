
angular.module('starter')

.controller('DashboardCtrl', function($scope, $rootScope, $stateParams, $ionicSlideBoxDelegate, $timeout, $window, HttpService, LocalService, $location, GAService) {
	
	// Init Server data
	HttpService.init("getBlogs", "blogs", "logged_in", 5);
	HttpService.init("getProfile", "profile", "logged_in");

	// save user level here
	$scope.level = "";

	// determine current user level
	LocalService.get("profile", function(profile){
		var level = parseInt(profile.level.value) + 3;
		$scope.level = (level > 3) ? 3 : level;
	});

	$scope.onCodesClick = function(){
		$location.path("/app/codes");
		GAService.trackEvent("Codes", "Dashboard", "clicked", null);
	}
});
