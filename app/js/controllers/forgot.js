
angular.module('starter')

.controller('ForgotCtrl', function($scope, $stateParams, $rootScope, $location, HttpService, $ionicPopup, $ionicNavBarDelegate, $ionicSideMenuDelegate, GAService) {

	$scope.forgotData = {};

	$scope.doRecover = function() {
		GAService.trackEvent("Recover", "Password", "clicked", null);

	  	HttpService.forgotPassword($scope.forgotData.email, function(data){
	  		GAService.trackEvent("Recover", "Password", "success", null);

			// redirect to login and show alert
			$location.path("/login");
	  		$ionicPopup.alert({
	    		title: $rootScope.strings.forgot.alertTitle,
	    		template: $rootScope.strings.forgot.alertSentSuccess
	  		});
	  	});
	};
});
