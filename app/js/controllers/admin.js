
angular.module('starter')

.controller('AdminCtrl', function($scope, $rootScope, $ionicModal, HttpService, $location, $timeout, $ionicPopup) {
	// get Company Settings
	HttpService.init("getSettings", "settings", "logged_in");

	$scope.viewUser = function(idUser){
		$location.path("/admin/brandbassadors/"+idUser);
	}
});
