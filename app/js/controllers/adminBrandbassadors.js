
angular.module('starter')

.controller('AdminBrandbassadorsCtrl', function($scope, $rootScope, $stateParams, $ionicModal, $ionicScrollDelegate, HttpService, LocalService, $location, $timeout, $ionicPopup, $window, GAService) {
	HttpService.init("getUsers", "brandbassadors", "logged_in");
	$scope.usersNextPage = 2;
	$scope.noMoreUsers   = false;
	$scope.idUser = null;
	$scope.users = [];

	// generate days
	$scope.days = [];
	for (var i = 1; i <= 31; i++) {
	  $scope.days.push(i);
	};

	// generate months
	$scope.months = $rootScope.strings.months;

	// generate years
	$scope.years = [];
	for (var i = 1960; i <= 2010; i++) {
	  $scope.years.push(i);
	};  

	$scope.hasMoreUsers = function(){
		return $scope.noMoreUsers ? false : true;
	}

	$scope.loadMoreUsers = function(){

		HttpService.getUsers({
			limit : 20,
			page  : $scope.usersNextPage 
		}, function(response){
			if (response.data.users.length > 0) {
				$scope.usersNextPage++;
			} else {
				$scope.noMoreUsers = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	if (angular.isDefined($stateParams.idUser)) {
		HttpService.getSocialNetworks($stateParams.idUser, function(response){
			$rootScope.$on("user_"+$stateParams.idUser, function(){
				$scope.users["_"+$stateParams.idUser].networks = response.data.networks;
			});
		});

		HttpService.getUser($stateParams.idUser, function(response){
			$scope.users["_"+$stateParams.idUser] = response.data;
			$scope.users["_"+$stateParams.idUser].gender = (parseInt($scope.users["_"+$stateParams.idUser].gender) == 1) ? $rootScope.strings.profile.labelMale : $rootScope.strings.profile.labelFemale;

			$scope.idUser = $stateParams.idUser;
			$rootScope.$broadcast("user_"+$stateParams.idUser);
		});

		console.log($scope.currentUser);
	}

	$scope.showUserNetworkFollowers = function(network){
		if (angular.isDefined($scope.users["_"+$stateParams.idUser]) && angular.isDefined($scope.users["_"+$stateParams.idUser].networks) && network in $scope.users["_"+$stateParams.idUser].networks) {
			return true;
		}

		return false;
	}

	$scope.onUserClick = function(brandbassador) {
		$rootScope.currentUser = brandbassador;
	}

	$scope.onApproveClick = function(idUser){
		GAService.trackEvent("Admin", "User Approve", "clicked", null);

		$ionicPopup.confirm({
		  title: $rootScope.strings.admin.brandbassadors.confirmTitleApprove,
		  template: $rootScope.strings.admin.brandbassadors.confirmApprove
		}).then(function(confirm){
			if (confirm) {

				GAService.trackEvent("Admin", "User Approve", "confirmed", null);

				// send Payment request to server
				HttpService.approveUser(idUser, function(response){

					GAService.trackEvent("Admin", "User Approve", "success", null);

					$ionicPopup.alert({
					  title: $rootScope.strings.admin.brandbassadors.alertTitleApproveCompleted,
					  template: $rootScope.strings.admin.brandbassadors.alertApproveCompleted 
					});  

					if (angular.isDefined($rootScope.currentUser)) {
						$rootScope.currentUser.is_active = "1";
					}

					HttpService.getUsers(null, function(){
						$scope.usersNextPage = 2;
						$scope.noMoreUsers   = false;
					});

				});
			}
		});
	}
});
