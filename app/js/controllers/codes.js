
angular.module('starter')

.controller('CodesCtrl', function($rootScope, $scope, $stateParams, $window, LocalService, HttpService, $ionicPopup, $timeout, GAService) {

	// Init Server Data
	HttpService.init("getDiscountCodes", "codes", "logged_in");
	HttpService.init("getProfile", "profile", "logged_in");

	var scroll_view, height;
	var window_height = $window.innerHeight;

	$scope.codeData = {};

	LocalService.get("profile", function(profile){

		// init layout and labels
		if (profile.level.value > -2) {
			$scope.code_type = $rootScope.strings.codes.labelMultiuseType;
			$scope.codeData.type = "multiUse";
			height  = window_height - 389;
		} else {
			$scope.code_type = $rootScope.strings.codes.labelSingleuseType;
			$scope.codeData.type = "singleUse";
			height  = window_height - 327;
		}

		scroll_view = angular.element(document.getElementsByClassName("table-body")); 
		scroll_view.css({
			"min-height" : height+"px"
		});
	});

	LocalService.get("discountCodes", function(codes){
		for (var i = codes.length - 1; i >= 0; i--) {
			codes[i].expires = codes[i].expires + "000";
		};

		$rootScope.discountCodes = codes;
	});

	// display popup after code creation
	$scope.showCreatedPopup = function(code){
		$scope.code = code;
		$ionicPopup.show({
		    title: $rootScope.strings.codes.labelCreatedSuccess,
		    template: "<p>"+$rootScope.strings.codes.labelShare+"</p>"+
		    			"<div class='button button-block fb' ng-click='onFBShareClick(code)'>"+$rootScope.strings.codes.labelFBShare+"</div>"+
		    			"<a href='https://twitter.com/intent/tweet?text={{strings.codes.labelShareDescription}}%20{{code}}&amp;tw_p=tweetbutton&amp;url=http://www.brandbassador.com' class='button button-block tw' target='_blank'>"+$rootScope.strings.codes.labelTWShare+"</a>",
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Ok' }
		    ]
		});
	}

	$scope.onFBShareClick = function(code){
		// show sharing popup
		FB.ui({
           	method: 'feed',
           	link: "http://www.brandbassador.com",
           	description: $rootScope.strings.codes.labelShareDescription + " " + code

        }, function(response) {

        });
	}

	$scope.onTWShareClick = function(){
		console.log("TW SHARE");
	}

	// creates a Discount Code in the PieceKeeper System
	$scope.createCode = function(){
		GAService.trackEvent("Code", "Creation", "clicked", null);

		HttpService.postCode($scope.codeData, function(data){
			GAService.trackEvent("Code", "Creation", "success", null);
			$scope.showCreatedPopup($scope.codeData.code);

			// refresh discount codes Data
			HttpService.getDiscountCodes();

			$scope.codeData.code = "";
			$scope.codeData.link = "";
		});
	};
});
