
angular.module('starter')

.controller('AdminDashboardCtrl', function($scope, $rootScope, $ionicModal, HttpService, $location, $timeout, $ionicPopup, $window) {
	HttpService.init("getAdminStats", "adminStats", "logged_in");
	HttpService.init("getAdminNotifications", "adminNotifications", "logged_in");

	$scope.notificationsNextPage = 2;
	$scope.noMoreNotifications = false;

	$scope.hasMoreNotifications = function(){
		return $scope.noMoreNotifications ? false : true;
	}

	$scope.loadMoreNotifications = function(){

		HttpService.getAdminNotifications({
			limit : 20,
			page  : $scope.notificationsNextPage 
		}, function(response){
			if (response.data.notifications.length > 0) {
				$scope.notificationsNextPage++;
			} else {
				$scope.noMoreNotifications = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.onNotificationClick = function(notification){
		if (notification.type == "mission") {
			$location.path("/admin/missions/"+notification.id_mission+"/"+notification.id_user);
		}
		else if (notification.type == "brandbassador") {
			$location.path("/admin/brandbassadors/"+notification.id_user);
		}
	}
});
