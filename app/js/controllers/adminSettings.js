
angular.module('starter')

// directive for settings accordion reposition after toggle goal
.directive("settingsRepeat", function($ionicScrollDelegate, $timeout){
	return function(scope, elements, attrs){
		scope.$watch(function(){
			return elements;
		}, function(elements){

			for (var i = elements.length - 1; i >= 0; i--) {
				var el = elements[i]

				var value = el.lastElementChild.className;
				if (value == "item-accordion item-text-wrap item") {
					$ionicScrollDelegate.resize();
					$ionicScrollDelegate.scrollTop(true);
				}

				var item_accordion = el.childNodes[3];

				angular.element(item_accordion).bind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function(event){

				});
			};

		});
	}
})

.controller('AdminSettingsCtrl', function($scope, $rootScope, $ionicModal, HttpService, $location, $timeout, $ionicPopup, $ionicScrollDelegate, LocalService, GAService) {
	HttpService.init("getSettings", "settings", "logged_in");

	$scope.companySettings = {};
	$scope.current_active = 0;
	$scope.items = [{
		title : $rootScope.strings.admin.settings.labelCompany,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoCompanyName,
			value : "company_name",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoCompanyPhone,
			value : "company_phone"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoCompanyAddress1,
			value : "company_address_1"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoCompanyAddress2,
			value : "company_address_2"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelRegistrationUrl,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoRegistrationUrl,
			value : "registration_url",
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelColors,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoPrimaryColor,
			value : "primary_color",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoSecondaryColor,
			value : "secondary_color"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelCodes,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoSingleUseLimit,
			value : "24h_discounts_per_user",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoMultiUseLimit,
			value : "multiuse_codes_per_week"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelCommission,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoLevel1Commission,
			value : "level1_commission",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoLevel2Commission,
			value : "level2_commission"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoLevel3Commission,
			value : "level3_commission"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoSlaveCommission,
			value : "slave_commission"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelPayouts,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoMinPayout,
			value : "minimum_commission_payout",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoLimitPayout,
			value : "payout_requests_per_month"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelLevels,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoBonusPointsLevel2,
			value : "bonus_points_level_2",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoBonusPointsLevel3,
			value : "bonus_points_level_3"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoTotalPointsLevel2,
			value : "total_points_level_2"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoTotalPointsLevel3,
			value : "total_points_level_3"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoSalesLevel2,
			value : "sales_level_2"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoSalesLevel3,
			value : "sales_level_3"
		}]
	}, {
		title : $rootScope.strings.admin.settings.labelBestPerformers,
		inputs : [{
			label : $rootScope.strings.admin.settings.inputInfoBestPerformersDays,
			value : "best_performers_days_interval",
		}, {
			label : $rootScope.strings.admin.settings.inputInfoBestPerformersSize,
			value : "best_performers_list_size"
		}, {
			label : $rootScope.strings.admin.settings.inputInfoBestPerformersPoints,
			value : "best_performers_default_points"
		}]
	},];

	for (var i = $scope.items.length - 1; i >= 0; i--) {
		$scope.items[i].arrow_type = "ion-arrow-right-b";
	};

	$scope.shownItem = $scope.items[0];
	$scope.items[0].arrow_type = "ion-arrow-down-b";

	LocalService.get("settings", function(settings){
		$scope.companySettings = settings;
	});


	/*
	 * open an item
	 */
	$scope.toggleItem = function(index) {
		if (index == $scope.current_active) {
			return;
		}

		// get info tag height
		var info_tag = angular.element(document.getElementById("settings-info"));
		var offsetY = info_tag[0].offsetHeight;

		// get height of every item above current one
		for (i = 0; i < index; i++) {
			var el = angular.element(document.getElementById("item-"+i));
			offsetY += el[0].offsetHeight;
		}

		$ionicScrollDelegate.$getByHandle("settings").resize();
		$ionicScrollDelegate.$getByHandle("settings").scrollTo(0, offsetY, true);

	    $scope.shownItem = $scope.items[index];
	    $scope.items[index].arrow_type = "ion-arrow-down-b";
	    $scope.items[$scope.current_active].arrow_type = "ion-arrow-right-b";

	    $scope.current_active = index;
	};

	$scope.isItemShown = function(index) {
	   	return $scope.shownItem === $scope.items[index];
	};

	$scope.onSaveClick = function(){
		GAService.trackEvent("Admin", "Settings", "save-clicked", null);

		$ionicPopup.confirm({
		  title: $rootScope.strings.admin.settings.confirmTitleSettings,
		  template: $rootScope.strings.admin.settings.confirmSettings
		}).then(function(confirm){
			if (confirm) {
				GAService.trackEvent("Admin", "Settings", "save-confirmed", null);

				// send request to server
				HttpService.editSettings($scope.companySettings, function(data){
					GAService.trackEvent("Admin", "Settings", "save-success", null);

					$ionicPopup.alert({
					  title: $rootScope.strings.admin.settings.alertTitleSettings,
					  template: $rootScope.strings.admin.settings.alertSettings
					}); 
				});
			}
		});
	}
});
