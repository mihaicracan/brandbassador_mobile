
angular.module('starter')

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, HttpService, $location, $timeout, $ionicPopup) {

	// Removes all anchors that contain images and keeps the images.
	// This way they won't be clickable
	$rootScope.removeImageAnchors = function(text){
		var re = /<a.*>(<img.*)<\/a>/g;
		var m;

		while ((m = re.exec(text)) !== null) {
		    if (m.index === re.lastIndex) {
		        re.lastIndex++;
		    }
		    text = text.replace(m[0], m[1]);
		}

		return text;
	}
	
	$scope.showAlert = function(title, errors){
		var message = "";

		for (var i in errors) {
			message += "<p>" + errors[i] + "</p>";
		}

		var alertPopup = $ionicPopup.alert({
		    title: title,
		    template: message
		});
	}

	// get Company Settings
	HttpService.init("getSettings", "settings", "logged_in");
	HttpService.init("getProfile", "profile", "logged_in");
});
