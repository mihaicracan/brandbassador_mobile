
angular.module('starter')

.controller('ResetCtrl', function($scope, $stateParams, $rootScope, $location, HttpService, ErrorService, $stateParams, $ionicPopup, $ionicNavBarDelegate, $ionicSideMenuDelegate, GAService) {

	$scope.resetData = {};

	$scope.doReset = function() {
		GAService.trackEvent("Reset", "Password", "clicked", null);

		$scope.resetData.hash = $stateParams.hash;

  		if (angular.isUndefined($scope.resetData.password) || $scope.resetData.password.length == 0) {
  			ErrorService.catch("requiredNewPassword");
  		}

  		else if (angular.isUndefined($scope.resetData.confirm_password) || $scope.resetData.password != $scope.resetData.confirm_password) {
  			ErrorService.catch("invalidPasswordConfirm");
  		}

  		else {
  			// creating account after all images have been uploaded
  			HttpService.resetPassword($scope.resetData, function(data){
  				GAService.trackEvent("Reset", "Password", "success", null);

	    		$ionicPopup.alert({
		      		title: $rootScope.strings.reset.alertTitleResetSuccess,
		      		template: $rootScope.strings.reset.alertResetSuccess
	    		});

	    		$location.path("/login");
  			});
  		}
	};

	$scope.showAlert = function(errors){
	  var message = "";

	  for (var i in errors) {
	    message += "<p>" + errors[i] + "</p>";
	  }

	  var alertPopup = $ionicPopup.alert({
	      title: $rootScope.strings.reset.alertTitleReset,
	      template: message
	  });
	}
});
