
angular.module('starter')

.controller('SalesteamCtrl', function($scope, $rootScope, $stateParams, $window, LocalService, HttpService, ErrorService, $ionicPopup, GAService) {

	// Init Server Data
	HttpService.init("getSalesTeam", "salesteam", "logged_in");

	$scope.invitationData = {};

	LocalService.get("salesteam", function(salesteam){

		for (var i = salesteam.invitations.length - 1; i >= 0; i--) {
			if (salesteam.invitations[i].approved == 1) {
				salesteam.invitations[i].status = $rootScope.strings.salesteam.labelStatusAccepted;
			} else {
				salesteam.invitations[i].status = $rootScope.strings.salesteam.labelStatusPending;
			}
		}
	});

	$scope.sendInvitation = function(){
		GAService.trackEvent("Invitation", "Send", "clicked", null);

		if (angular.isUndefined($scope.invitationData.email) || $scope.invitationData.email.length == 0) {
			ErrorService.catch("requiredEmail");
		} else {
			$ionicPopup.confirm({
			  title: $rootScope.strings.salesteam.confirmTitleInvite,
			  template: $rootScope.strings.salesteam.confirmInvite
			}).then(function(confirm){
				if (confirm) {
					GAService.trackEvent("Invitation", "Send", "confirmed", null);

					// send invitation request to server
					HttpService.sendTeamInvitation($scope.invitationData, function(data){
						GAService.trackEvent("Invitation", "Send", "success", null);

						$scope.invitationData = {};

						HttpService.getSalesTeam(function(){
							$ionicPopup.alert({
							  title: $rootScope.strings.salesteam.alertTitleInvite,
							  template: $rootScope.strings.salesteam.alertInviteSuccess
							});  
						});

					});
				}
			});
		}
	}
});
