
angular.module('starter')

.controller('LoginCtrl', function($scope, $stateParams, $rootScope, HttpService, ErrorService, GAService, $ionicPopup, $location, $ionicNavBarDelegate, $ionicSideMenuDelegate, $timeout) {

	$scope.loginData = {};

	/*
	* Tries user authentication based on email and password
	*/
	$scope.doLogin = function() {
		GAService.trackEvent("Login", "Email", "clicked", null);

		// check credentials on server
	  	HttpService.login($scope.loginData, function(data){
	  		GAService.trackEvent("Login", "Email", "success", null);

	  		// on success redirect to dashboard
	    	$location.path("/app/dashboard");

	  	}, function(data){
	  		// catch errors in GA
	    	if (angular.isDefined(data.errors["invalidLoginCredentials"])) {
	    		GAService.trackEvent("Login", "Email", "invalid-credentials", null);
	    	} 
	    	else if (angular.isDefined(data.errors["maximumLoginAttemptsExceeded"])) {
	    		GAService.trackEvent("Login", "Email", "blocked-ip", null);
	    	}
	  	});
	};

	// Tries Facebook login
	$scope.doFBLogin = function(){
		GAService.trackEvent("Login", "Facebook", "clicked", null);

		FB.getLoginStatus(function(response){
		   	if (response.status != "connected") {
		    	FB.login(function(response){
		      		if (response.status == "connected") {
		      			// send Facebook access token to server
		        		$scope.handleFBLogin(response.authResponse.accessToken);
		      		} else {
		        		ErrorService.catch("facebookError");
		      		}
		    	}, {scope: 'email, user_about_me, public_profile, user_location, user_friends, manage_pages, user_birthday, user_education_history'});
		  	} else {
		  		// send Facebook access token to server
		  		$scope.handleFBLogin(response.authResponse.accessToken);
		  	}
		});
	};

	// Send facebook access token to server
	$scope.handleFBLogin = function(accessToken){
		HttpService.FBLogin({
			access_token: accessToken
		}, function(data){
			GAService.trackEvent("Login", "Facebook", "success", null);
			$location.path("/app/dashboard");
		}, function(data) {
			GAService.trackEvent("Login", "Facebook", "not-registered", null);
			// $location.path("/register");
		});
	}
});
