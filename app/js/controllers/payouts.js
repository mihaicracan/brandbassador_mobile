
angular.module('starter')

.controller('PayoutsCtrl', function($scope, $stateParams, $rootScope, $window, LocalService, HttpService, ErrorService, $ionicPopup, $timeout, GAService) {

	$scope.payoutsData = {};
	$scope.payoutsData.giftcard_checked = false;
	$scope.payoutsData.paypal_checked   = false;

	// Init Payouts from Server
	HttpService.init("getPayouts", "payouts", "logged_in");
	HttpService.init("getProfile", "profile", "logged_in");

	// function that rounds a float number
	$scope.toFixed = function(value, precision) {
        var precision = precision || 0,
        neg = value < 0,
        power = Math.pow(10, precision),
        value = Math.round(value * power),
        integral = String((neg ? Math.ceil : Math.floor)(value / power)),
        fraction = String((neg ? -value : value) % power),
        padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');

        return precision ? integral + '.' +  padding + fraction : integral;
    }

	LocalService.get("payouts", function(payouts){
		$scope.payoutsData.amount   = payouts.totals.commission;
		$scope.payoutsData.giftcard = payouts.totals.giftcard;
		
		// dynamic checkbox messages
		$scope.$watch("payoutsData.amount", function(value){
			var value = (parseFloat(payouts.totals.extra_giftcard_percentage)+100)/100*parseFloat(value);
			$scope.payoutsData.giftcard = $scope.toFixed(value, 2);
		});

		// init payouts details
		for (var i in $rootScope.payouts.payments) {
			$rootScope.payouts.payments[i].created += "000"; 

			if ($rootScope.payouts.payments[i].approved == "-1") {
				$rootScope.payouts.payments[i].status = $rootScope.strings.payouts.labelStatusRejected;
			}
			else if ($rootScope.payouts.payments[i].approved == "0") {
				$rootScope.payouts.payments[i].status = $rootScope.strings.payouts.labelStatusWaiting;
			}
			else {
				$rootScope.payouts.payments[i].status = $rootScope.strings.payouts.labelStatusApproved;
			}
		}
	});

	// simulate radio boxes
	$scope.$watch("payoutsData.paypal_checked", function(checked){
		if (checked) {
			$scope.payoutsData.giftcard_checked = false;
		}
	});
	$scope.$watch("payoutsData.giftcard_checked", function(checked){
		if (checked) {
			$scope.payoutsData.paypal_checked = false;
		}
	});

	// Display proper messages and send Payment Request to server
	$scope.requestPayment = function(){

		GAService.trackEvent("Payment", "Request", "clicked", null);

		// check if option is selected
		if (!$scope.payoutsData.paypal_checked && !$scope.payoutsData.giftcard_checked) {
			ErrorService.catch("invalidPaymentMethod");   
		} 

		// check if PayPal email is set
		else if ($scope.payoutsData.paypal_checked && (angular.isUndefined($rootScope.profile.paypal.email) || $scope.profile.paypal.email === null || $rootScope.profile.paypal.email.length == 0)) {
			ErrorService.catch("requiredPayPalEmail");  

		} else {

			var message = "";

			// set message confirmation based on payout option
			if ($scope.payoutsData.giftcard_checked) {
				message = $rootScope.strings.payouts.confirmPaymentGiftcard;
			} else {
				message = $rootScope.strings.payouts.confirmPaymentPayPal;
			}

			$ionicPopup.confirm({
			  title: $rootScope.strings.payouts.confirmTitlePayment,
			  template: message
			}).then(function(confirm){
				if (confirm) {

					// send Payment request to server
					HttpService.requestPayment({
						amount : $scope.payoutsData.amount,
						payment_method : $scope.payoutsData.giftcard_checked ? "giftcard" : "paypal" 
					}, function(data){
						GAService.trackEvent("Payment", "Request", "success", null);
						$scope.payoutsData = {};

						// refresh payments information
						HttpService.getPayouts();

						if ($scope.payoutsData.giftcard_checked) {
							message = $rootScope.strings.payouts.alertRequestSuccessGiftcard;
						} else {
							message = $rootScope.strings.payouts.alertRequestSuccessPayPal;
						}

						$ionicPopup.alert({
						  title: $rootScope.strings.payouts.alertTitleRequest,
						  template: message 
						});  
					});
				}
			});
		}
	}


	// Display payment value if it is valid
	$scope.showPaymentValue = function(){
		if (angular.isDefined($scope.payoutsData.amount) && $scope.payoutsData.amount != '' && !isNaN($scope.payoutsData.amount)) {
			return true;
		}

		return false;
	}


	// Update PayPal email on server
	$scope.updatePayPal = function(){
		if (angular.isUndefined($scope.profile.paypal.email) || $scope.profile.paypal.email === null || $scope.profile.paypal.email.length == 0) {
			ErrorService.catch("requiredPayPalEmail");
		} else {
			$ionicPopup.confirm({
			  title: $rootScope.strings.payouts.alertTitlePayPal,
			  template: $rootScope.strings.payouts.confirmPayPal
			}).then(function(confirm){
				if (confirm) {

					// send request to server
					HttpService.updateProfile({
						paypal_email: $scope.profile.paypal.email
					}, function(data){
						$ionicPopup.alert({
						  title: $rootScope.strings.payouts.alertTitlePayPal,
						  template: $rootScope.strings.payouts.alertPayPalSuccess
						}); 
					});
				}
			});
		}
	}
});
