
angular.module('starter')

.controller('CompaniesCtrl', function($rootScope, $scope, $state, $stateParams, $window, LocalService, HttpService, $ionicPopup, $timeout, GAService) {

	// Init Server Data
	HttpService.init("getCompanies", "companies", "logged_in");

	$scope.initSelector = function(companies){

		if (companies.length * 100 > $window.innerWidth) {
			$scope.companySelectorItemWidth = 100;
			$scope.companySelectorWrapperWidth = 102 * companies.length;
		} else {
			$scope.companySelectorItemWidth = $window.innerWidth / companies.length;
			$scope.companySelectorWrapperWidth = $window.innerWidth;
		}

	}

	$scope.changeCompany = function(id_company){
		GAService.trackEvent("Company", "Change", "clicked", null);

		if (parseInt(id_company) != parseInt($rootScope.currentCompany)) {
			HttpService.setCompany(id_company, function(){
				GAService.trackEvent("Company", "Change", "success", null);

				HttpService.reloadInitialized();
			});
		}
	}

	LocalService.get("companies", function(companies){
		$scope.initSelector(companies);
	});
	
});
