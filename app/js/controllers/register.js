
angular.module('starter')

.controller('RegisterCtrl', function($scope, $stateParams, $rootScope, $location, HttpService, $ionicPopup, $ionicModal, $ionicNavBarDelegate, $ionicSideMenuDelegate, GAService) {
	
	// Get Server Data
	HttpService.init("getCountries", "countries");
	HttpService.init("getStates", "states");

	// if invitation is sent, save it on server
	if (angular.isDefined($stateParams.invitation) && $stateParams.invitation.length > 0 && angular.isDefined($stateParams.company) && $stateParams.company.length > 0) {
		HttpService.setInvitation({
			invitation: $stateParams.invitation,
			id_company: $stateParams.company
		});
	}

	// Tries Facebook login
	$scope.doFBRegister = function(){
		GAService.trackEvent("Register", "Facebook", "clicked", null);
		FB.getLoginStatus(function(response){
		   	if (response.status != "connected") {
		    	FB.login(function(response){
		      		if (response.status == "connected") {
		      			// send Facebook access token to server
		        		$scope.handleFBRegister(response.authResponse.accessToken);
		      		}
		    	}, {scope: 'email, user_about_me, public_profile, user_location, user_friends, manage_pages, user_birthday, user_education_history'});
		  	} else {
		  		// send Facebook access token to server
		  		$scope.handleFBRegister(response.authResponse.accessToken);
		  	}
		});
	};

	// Send facebook access token to server
	$scope.handleFBRegister = function(accessToken){
		HttpService.FBRegister({
			access_token: accessToken
		}, function(data){
			GAService.trackEvent("Register", "Facebook", "success", null);
			$location.path("/social");
		});
	}

	
});
