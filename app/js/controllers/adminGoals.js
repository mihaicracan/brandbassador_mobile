
angular.module('starter')

.controller('AdminGoalsCtrl', function($scope, $rootScope, $ionicModal, $stateParams, HttpService, $location, $timeout, $ionicPopup, $window, GAService) {
	HttpService.init("getAdminGoals", "adminGoals", "logged_in");

	$scope.goalsNextPage = 2;
	$scope.winnersNextPage = 2;
	$scope.noMoreGoals   = false;
	$scope.noMoreWinners = false;
	$scope.idGoal        = null;
	$scope.winners       = [];

	$rootScope.goalDetails = angular.copy($rootScope.currentGoal);

	$scope.hasMoreGoals = function(){
		return $scope.noMoreGoals ? false : true;
	}

	$scope.hasMoreWinners = function(){
		return $scope.noMoreWinners ? false : true;
	}

	$scope.loadMoreGoals = function(){

		HttpService.getAdminGoals({
			limit : 20,
			page  : $scope.goalsNextPage 
		}, function(response){
			if (response.data.goals.length > 0) {
				$scope.goalsNextPage++;
			} else {
				$scope.noMoreGoals = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.loadMoreWinners = function(){

		HttpService.getAdminGoalWinners({
			id_goal : $stateParams.idGoal,
			limit : 20,
			page  : $scope.winnersNextPage 
		}, function(response){
			if (response.data.winners.length > 0) {
				$scope.winnersNextPage++;
			} else {
				$scope.noMoreWinners = true;
			}

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.viewGoal = function(goal){

		GAService.trackEvent("Admin", "Goal", "view-clicked", null);

		goal.id_goal = goal.id;
		$rootScope.currentGoal = goal;
	}

	$scope.editGoal = function(idGoal) {
		GAService.trackEvent("Admin", "Goal", "edit-clicked", null);

		$location.path("admin/goals/edit/"+idGoal);
	}

	$scope.onSaveClick = function(){
		GAService.trackEvent("Admin", "Goal", "save-clicked", null);

		$ionicPopup.confirm({
		  title: $rootScope.strings.admin.goalEdit.confirmTitleEdit,
		  template: $rootScope.strings.admin.goalEdit.confirmEdit
		}).then(function(confirm){
			if (confirm) {

				GAService.trackEvent("Admin", "Goal", "save-confirmed", null);

				// send request to server
				HttpService.editGoal($rootScope.goalDetails, function(){

					GAService.trackEvent("Admin", "Goal", "save-success", null);

					HttpService.getAdminGoals({
						limit: 20,
						page: 1
					}, function(){
						$scope.goalsNextPage = 2;
					});
				});
			}
		});
	}

	if (angular.isDefined($stateParams.idGoal)) {

		HttpService.getAdminGoalWinners({
			id_goal : $stateParams.idGoal,
			limit : 20,
			page : 1
		}, function(response){
			$scope.winners["_"+$stateParams.idGoal] = response.data.winners;
			$scope.idGoal = $stateParams.idGoal;
		});
	}
});
