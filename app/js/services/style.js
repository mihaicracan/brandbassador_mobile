
angular.module('starter')

.factory('StyleService', function($rootScope) {
	var StyleService = {};

	StyleService.style = null;
	StyleService.primaryColorRules = {
		"a" : ["color"],

		".button.button-positive" : ["background-color"],
		".button.button-positive.active, .button.button-positive.activated" : ["background-color#-0.25"],
		
		"ion-view.custom input[type='text']:focus, ion-view.custom input[type='password']:focus, ion-view.custom textarea:focus, ion-modal-view.custom input[type='text']:focus, ion-modal-view.custom input[type='password']:focus, ion-modal-view.custom textarea:focus" : ["border-bottom-color"],
		"ion-view.custom input[type='text']:focus ~ label, ion-view.custom input[type='text']:valid ~ label, ion-view.custom input[type='password']:focus ~ label, ion-view.custom input[type='password']:valid ~ label, ion-view.custom textarea:focus ~ label, ion-view.custom textarea:valid ~ label, ion-modal-view.custom input[type='text']:focus ~ label, ion-modal-view.custom input[type='text']:valid ~ label, ion-modal-view.custom input[type='password']:focus ~ label, ion-modal-view.custom input[type='password']:valid ~ label, ion-modal-view.custom textarea:focus ~ label, ion-modal-view.custom textarea:valid ~ label" : ["color"],
		"ion-view.custom .input-label, ion-modal-view.custom .input-label" : ["color", "border-bottom-color"],
		"ion-view.custom .table-title span, ion-modal-view.custom .table-title span" : ["color", "border-bottom-color"],
		"ion-view.custom .companiesSelector ion-scroll .wrapper .company.active, ion-modal-view.custom .companiesSelector ion-scroll .wrapper .company.active" : ["border-bottom-color"],
		
		"ion-view.dashboard .main-info .load-bar-wrapper .load-bar-value" : ["background-color"],
		"ion-view.dashboard .row.earnings" : ["background-color"],
		"ion-view.dashboard .row.ranking .global .load-bar-wrapper .load-bar-value, ion-view.dashboard .row.points .global .load-bar-wrapper .load-bar-value" : ["background-color"],
		"ion-view.dashboard .row.ranking .total .load-bar-wrapper .load-bar-value, ion-view.dashboard .row.points .total .load-bar-wrapper .load-bar-value" : ["background-color"],
		
		"ion-view.levels ion-content .list .item .wrapper .checkmarker .sign" : ["color"],
		"ion-view.levels ion-content .list .item .wrapper .checkmarker .sign.sign-cover" : ["border-color"],
		"ion-view.levels ion-content .list .item .wrapper .subtitle" : ["color"],
		
		"ion-view.goals .list .item.item-accordion .ranking .details" : ["background-color"],
		
		"ion-modal-view.register .scroll label.checkbox input:checked:before, ion-modal-view.register .scroll label.checkbox input:checked + .checkbox-icon:before" : ["background-color", "border-color"],
		"ion-modal-view.register .scroll .submit button" : ["background-color"],
		
		"ion-view.payouts .request label.checkbox input:checked + .checkbox-icon:before, ion-view.payouts .request label.checkbox input:checked:before" : ["background-color", "border-color"],
		
		"ion-view.admin.brandbassadorView .stats .row .col.codes" : ["background-color"],
		"ion-view.admin.brandbassadorView .stats .row .col.commission" : ["background-color"],
		
		"ion-view.admin.adminDashboard .adminStats .row .col.earnings" : ["background-color"],
		"ion-view.admin.adminDashboard .notifications .notification" : ["background-color"],

	};

	StyleService.secondaryColorRules = {
		".button.button-calm" : ["background-color"],
		".button.button-calm.active, .button.button-calm.activated" : ["background-color#-0.25"],

		"ion-view.dashboard .row.earnings .giftcard" : ["background-color"],

		"ion-view.admin.brandbassadorView .stats .row .col.earnings" : ["background-color"],
		
		"ion-view.admin.adminDashboard .adminStats .row .col.commission" : ["background-color"],
	};

	StyleService.createSheet = function() {
		// Create the <style> tag
		var style = document.createElement("style");

		// Add a media (and/or media query) here if you'd like!
		style.setAttribute("type", "text/css");

		// WebKit hack :(
		style.appendChild(document.createTextNode(""));

		// Add the <style> element to the page
		document.head.appendChild(style);

		this.style = style;
	}

	StyleService.deleteSheet = function() {
		if (this.style != null) {
			this.style.parentNode.removeChild(this.style);
		}
	}

	StyleService.resetSheet = function(){
		this.deleteSheet();
		this.createSheet();
	}

	StyleService.addCSSRule = function (selector, rules, index) {
		if("insertRule" in this.style.sheet) {
			this.style.sheet.insertRule(selector + "{" + rules + "}", index);
		}
		else if("addRule" in this.style.sheet) {
			this.style.sheet.addRule(selector, rules, index);
		}
	}

	StyleService.setColorRules = function(newColor, rules){
		for (var selector in rules) {
			var properties = rules[selector];

			for (var i in properties) {
				var property = properties[i];
				var color = newColor;

				if (property.match(/#/)) {
					var data = property.split("#");
					property = data[0];
					color    = this.shadeColor(newColor, parseFloat(data[1]));
				}

				this.addCSSRule(selector, property + ":" + color);
			}
		}
	}

	StyleService.changeColors = function(primaryColor, secondaryColor){
		if (primaryColor.length > 0 && secondaryColor.length > 0) {
			this.resetSheet();

			this.setColorRules(primaryColor, this.primaryColorRules);
			this.setColorRules(secondaryColor, this.secondaryColorRules);
		}
	}

	StyleService.shadeColor = function(hex, lum) {   
	   // validate hex string
	   hex = String(hex).replace(/[^0-9a-f]/gi, '');
	   if (hex.length < 6) {
	   	hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	   }
	   lum = lum || 0;

	   // convert to decimal and change luminosity
	   var rgb = "#", c, i;
	   for (i = 0; i < 3; i++) {
	   	c = parseInt(hex.substr(i*2,2), 16);
	   	c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
	   	rgb += ("00"+c).substr(c.length);
	   }

	   return rgb;
  	}

	return StyleService;
});
