
angular.module('starter')

.factory('HttpService', function($rootScope, $http, $ionicLoading, $ionicScrollDelegate, GAService, ErrorService, $interval, $location) {
	var HttpService = {};

	HttpService.active_requests = 0;
	HttpService.counters = {};
	HttpService.initialized = [];

	var PRODUCTION_KEY = "193575f7a22e0384ce041b7ecceaaed06c7fcfd5";
	var DEVELOPMENT_KEY = "9ca6b938f38401bc21c3302e519e67c02ea7f7533";

	$rootScope.auth_key   = PRODUCTION_KEY; // production
	// $rootScope.auth_key   = DEVELOPMENT_KEY; // development
	// $rootScope.server_url = "http://localhost/agvision/brandbassador/";
	$rootScope.server_url = "https://www.brandbassador.com/";

	// Initialize data from server
	// service - function to run
	// data - place requst only if $rootScope[data] is undefined
	// event - place request after event is triggered
	// arg - argument for request method
	HttpService.init = function(service, data, event, arg){
		if (angular.isDefined($rootScope[data])) {
			return;
		}

		if (angular.isDefined(event) && event.length > 0) {

			if (event == "logged_in") {
				if ((angular.isDefined($rootScope.logged_in) && $rootScope.logged_in) || $rootScope.auth_key == DEVELOPMENT_KEY) {
					HttpService[service](arg);
					HttpService.initialized.push({
						service : service,
						data : data,
						event : event,
						arg : arg
					});
				} else {
					$rootScope.$on(event, function(event, data){
						HttpService[service](arg);
						HttpService.initialized.push({
							service : service,
							data : data,
							event : event,
							arg : arg
						});
					});
				}
			}

			else if (event == "profile") {
				if (angular.isDefined($rootScope.profile)) {
					HttpService[service](arg);
					HttpService.initialized.push({
						service : service,
						data : data,
						event : event,
						arg : arg
					});
				} else {
					$rootScope.$on(event, function(event, data){
						HttpService[service](arg);
						HttpService.initialized.push({
							service : service,
							data : data,
							event : event,
							arg : arg
						});
					});
				}
			}
		} else {
			HttpService[service](arg);
			HttpService.initialized.push({
				service : service,
				data : data,
				event : event,
				arg : arg
			});
		}
	}

	HttpService.reload = function(service, data, event, arg){
		if (angular.isDefined($rootScope[data])) {
			delete $rootScope[data];
		}

		HttpService.init(service, data, event, arg);
	}

	HttpService.clearInitialized = function(){
		HttpService.initialized = [];
	}

	HttpService.reloadInitialized = function(){

		var initialized = HttpService.initialized;
		HttpService.clearInitialized();

		for (var i = 0; i < initialized.length; i++) {
			var data = initialized[i];
			HttpService.reload(data.service, data.data, data.event, data.arg);
		}
	}

	HttpService.markRequest = function(action){
		if (this.active_requests == 0) {
			HttpService.counters["total"] = HttpService.setTimeCounter();
			$ionicLoading.show();
		}

		this.active_requests++;

		HttpService.counters[action] = HttpService.setTimeCounter();
	}

	HttpService.unmarkRequest = function(action){
		if (this.active_requests > 0) {
			this.active_requests--;
		}

		if (this.active_requests == 0) {

			if (angular.isDefined(HttpService.counters["total"])) {
				var time = HttpService.counters["total"].getTime();
				GAService.trackRequestTime("total", time);
			}

			$ionicLoading.hide();
		}

		if (angular.isDefined(HttpService.counters[action])) {
			var time = HttpService.counters[action].getTime();
			GAService.trackRequestTime(action, time);
		}
	}

	HttpService.setTimeCounter = function(action){
		var counter = {};

		counter.value = 0;

		counter.count = function(){
			return $interval(function(){
				counter.value ++;
			}, 100);
		}

		counter.trigger = counter.count();

		counter.getTime = function(){
			GAService.trackRequestTime(action, counter.value);
			$interval.cancel(counter.trigger);
			return counter.value;
		}

		return counter;
	}

	HttpService.authNetworkRequest = function(authData, success, error){
		HttpService.markRequest("authNetworkRequest");
		var path = "";

		// build request Url based on oAuth step
		if (authData.step == "redirectUrl") {
			path = authData.step+"?key="+$rootScope.auth_key;
		} else if (authData.step == "getUser") {
			// build Request Url based on network
			if (authData.service == "twitter" || authData.service == "tumblr") {
				path = authData.step+"?key="+$rootScope.auth_key+"&oauth_token="+authData.oauth_token+"&oauth_verifier="+authData.oauth_verifier;
			} else if (authData.service == "instagram") {
				path = authData.step+"?key="+$rootScope.auth_key+"&code="+authData.code;
			} else if (authData.service == "pinterest") {
				path = authData.step+"?key="+$rootScope.auth_key+"&username="+authData.username;
			} else if (authData.service == "youtube") {
				path = authData.step+"?key="+$rootScope.auth_key+"&state="+authData.state+"&code="+authData.code;
			} else if (authData.service == "vine") {
				path = authData.step+"?key="+$rootScope.auth_key+"&userID="+authData.userID;
			}
		}

		// set redirect page
		if ($location.path().match(/profile/)) {
			path += "&redirectUrl=profile";
		} else {
			path += "&redirectUrl=social"
		}

		$http.get($rootScope.server_url+"api/auth/"+authData.service+"/"+path)
			.success(function(response){
				HttpService.unmarkRequest("authNetworkRequest");

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("authNetworkRequest");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.uploadProfileImage = function(index, file, success, error){

        var fd = new FormData();
        fd.append('file', file);
        $http.post($rootScope.server_url+"api/profile_image/"+index+"?key="+$rootScope.auth_key, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
	        .success(function(response){
	        	HttpService.unmarkRequest("uploadProfileImage");

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	        })
	        .error(function(response){
	        	ErrorService.catch(response);
	        	HttpService.unmarkRequest("uploadProfileImage");

	        	if (angular.isFunction(error)) {
	        		error(response);
	        	}
	        });
    }

	HttpService.uploadUrlProfileImage = function(index, url, success, error){

        $http.post($rootScope.server_url+"api/profile_image/"+index+"?key="+$rootScope.auth_key, {
        	imgUrl: url
        })
	        .success(function(response){
	        	HttpService.unmarkRequest("uploadProfileImage");

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	        })
	        .error(function(response){
	        	ErrorService.catch(response);
	        	HttpService.unmarkRequest("uploadProfileImage");

	        	if (angular.isFunction(error)) {
	        		error(response);
	        	}
	        });
    }

    HttpService.uploadMissionImage = function(id_mission, file, success, error){
    	HttpService.markRequest("uploadMissionImage");

        var fd = new FormData();
        fd.append('file', file);
        $http.post($rootScope.server_url+"api/missions/"+id_mission+"/image?key="+$rootScope.auth_key, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
	        .success(function(response){
	        	HttpService.unmarkRequest("uploadMissionImage");

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	        })
	        .error(function(response){
	        	ErrorService.catch(response);
	        	HttpService.unmarkRequest("uploadMissionImage");

	        	if (angular.isFunction(error)) {
	        		error(response);
	        	}
	        });
    }

	HttpService.checkLogin = function(success, error){
		HttpService.markRequest("checkLogin");

		$http.get($rootScope.server_url+"api/isLogged?key="+$rootScope.auth_key)
			.success(function(response){
				HttpService.unmarkRequest("checkLogin");

			  	$rootScope.$broadcast("logged_in");
			  	$rootScope.logged_in = true;
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("checkLogin");

				$rootScope.logged_in = false;
				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.login = function(loginData, success, error){
		HttpService.markRequest("login");

		$http.get($rootScope.server_url+"api/login?key="+$rootScope.auth_key+"&email="+loginData.email+"&password="+loginData.password)
		  	.success(function(response){
		  		HttpService.unmarkRequest("login");

	      		$rootScope.$broadcast("logged_in");
	      		$rootScope.logged_in = true;
	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("login");

				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.FBLogin = function(loginData, success, error){
		HttpService.markRequest("FBLogin");

		$http.post($rootScope.server_url+"api/FBLogin?key="+$rootScope.auth_key, loginData)
			.success(function(response){
				HttpService.unmarkRequest("FBLogin");

				$rootScope.$broadcast("logged_in");
				$rootScope.logged_in = true;

				if (angular.isFunction(success)) {
					success(response);
				}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("FBLogin");

				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.FBRegister = function(registerData, success, error){
		HttpService.markRequest("FBRegister");

		$http.post($rootScope.server_url+"api/fbregister?key="+$rootScope.auth_key, registerData)
			.success(function(response){
				HttpService.unmarkRequest("FBRegister");

				if (angular.isFunction(success)) {
					success(response);
				}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("FBRegister");

				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.linkSocialNetwork = function(networkData, success, error){
		HttpService.markRequest("linkNetwork");

		$http.post($rootScope.server_url+"api/linkNetwork?key="+$rootScope.auth_key, networkData)
			.success(function(response){
				HttpService.unmarkRequest("linkNetwork");

				if (angular.isFunction(success)) {
					success(response);
				}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("linkNetwork");

				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.register = function(registerData, success){
		HttpService.markRequest("register");

		$http.post($rootScope.server_url+"api/register?key="+$rootScope.auth_key, registerData)
		  	.success(function(response){
		  		HttpService.unmarkRequest("register");

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
		  	.error(function(response){
		  		ErrorService.catch(response);
		  		HttpService.unmarkRequest("register");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});

	}

	HttpService.completeRegistration = function(success, error){
		HttpService.markRequest("completeRegistration");

		$http.get($rootScope.server_url+"api/completeRegistration?key="+$rootScope.auth_key)
		  	.success(function(response){
		  		HttpService.unmarkRequest("completeRegistration");

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
		  		HttpService.unmarkRequest("completeRegistration");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.getUsers = function(usersData, success, error){

		if (!angular.isObject(usersData)) {
			usersData = {};
			usersData.limit = 20;
			usersData.page  = 1;
		}

		$http.get($rootScope.server_url+"api/users/"+usersData.limit+"/"+usersData.page+"?key="+$rootScope.auth_key)
		  	.success(function(response){

		  		if (angular.isUndefined($rootScope.brandbassadors)) {
		  			$rootScope.brandbassadors = [];
		  		}

		  		if (usersData.page > 1) {
		  			$rootScope.brandbassadors = $rootScope.brandbassadors.concat(response.data.users);
		  		} else {
		  			$rootScope.brandbassadors = response.data.users;
		  		}

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.getSettings = function(success, error){
		HttpService.markRequest("getSettings");

		$http.get($rootScope.server_url+"api/settings?key="+$rootScope.auth_key)
		  	.success(function(response){
		  		HttpService.unmarkRequest("getSettings");

		  		$rootScope.$broadcast("settings");
		  		$rootScope.settings = response.data.settings;

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
		  		HttpService.unmarkRequest("getSettings");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.editSettings = function(settingsData, success, error){
		HttpService.markRequest("saveSettings");

		$http.post($rootScope.server_url+"api/settings/edit?key="+$rootScope.auth_key, settingsData)
		  	.success(function(response){
		  		HttpService.unmarkRequest("saveSettings");

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
		  		HttpService.unmarkRequest("saveSettings");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.getCompanies = function(success, error){
		HttpService.markRequest("getCompanies");

		$http.get($rootScope.server_url+"api/companies?key="+$rootScope.auth_key)
		  	.success(function(response){
		  		HttpService.unmarkRequest("getCompanies");

		  		$rootScope.companies = response.data.companies;
		  		$rootScope.currentCompany = response.data.current;

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
		  		HttpService.unmarkRequest("getCompanies");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.setCompany = function(id_company, success, error){
		HttpService.markRequest("setCompany");

		$http.post($rootScope.server_url+"api/companies/set?key="+$rootScope.auth_key, {
			id_company : id_company
		})
		  	.success(function(response){
		  		HttpService.unmarkRequest("setCompany");

	      		if (angular.isFunction(success)) {
	      			success(response);
	      		}
			})
			.error(function(response){
				ErrorService.catch(response);
		  		HttpService.unmarkRequest("setCompany");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
			});
	}

	HttpService.logout = function(success, error){
		HttpService.markRequest("logout");

		$http.get($rootScope.server_url+"api/logout?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("logout");

		  		$rootScope.logged_in = false;
		  		if (angular.isFunction(success)) {
		  			success();
		  		}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("logout");

		  		if (angular.isFunction(error)) {
		  			error();
		  		}
			});
	}

	// get profile of the current logged user
	HttpService.getProfile = function(success, error){
		HttpService.markRequest("getProfile");

      	$http.get($rootScope.server_url+"api/profile?key="+$rootScope.auth_key)
	      	.success(function(response){
	        	HttpService.unmarkRequest("getProfile");
	        	$rootScope.profile = response.data;
	        	$rootScope.$broadcast("profile");
	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
      		.error(function(response){
      			ErrorService.catch(response);
      		  	HttpService.unmarkRequest("getProfile");

      	  		if (angular.isFunction(error)) {
      	  			error();
      	  		}
      		});
	}

	// get profile for a specific user (only admins)
	HttpService.getUser = function(id_user, success, error){
		HttpService.markRequest("getUser");

      	$http.get($rootScope.server_url+"api/profile/view/"+id_user+"?key="+$rootScope.auth_key)
	      	.success(function(response){
	        	HttpService.unmarkRequest("getUser");

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
      		.error(function(response){
      			ErrorService.catch(response);
      		  	HttpService.unmarkRequest("getUser");

      	  		if (angular.isFunction(error)) {
      	  			error();
      	  		}
      		});
	}

	HttpService.approveUser = function(id_user, success, error){
		HttpService.markRequest("approveUser");

      	$http.get($rootScope.server_url+"api/user/"+id_user+"/approve?key="+$rootScope.auth_key)
	      	.success(function(response){
	        	HttpService.unmarkRequest("approveUser");

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
      		.error(function(response){
      			ErrorService.catch(response);
      		  	HttpService.unmarkRequest("approveUser");

      	  		if (angular.isFunction(error)) {
      	  			error();
      	  		}
      		});
	}

	HttpService.getSocialNetworks = function(idUser, success, error){
		HttpService.markRequest("getSocialNetworks");
		var url;

		if (angular.isDefined(idUser) && idUser) {
			url = $rootScope.server_url+"api/socialNetworks/"+idUser+"?key="+$rootScope.auth_key;
		} else {
			url = $rootScope.server_url+"api/socialNetworks?key="+$rootScope.auth_key;
		}

      	$http.get(url)
	      	.success(function(response){
	        	HttpService.unmarkRequest("getSocialNetworks");

	        	if (angular.isUndefined(idUser) || !idUser) {
	        		$rootScope.networks = response.data.networks;
	        	}

	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
	      	.error(function(response){
	      		ErrorService.catch(response);
	      		HttpService.unmarkRequest("getSocialNetworks");

	      		if (angular.isFunction(error)) {
	      			error(response);
	      		}
	      	});
	}

	HttpService.getSalesTeam = function(success, error){
		HttpService.markRequest("getSalesTeam");

      	$http.get($rootScope.server_url+"api/salesTeam?key="+$rootScope.auth_key)
	      	.success(function(response){
	        	HttpService.unmarkRequest("getSalesTeam");
	        	$rootScope.salesteam = response.data;
	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
      		.error(function(response){
      			ErrorService.catch(response);
      		  	HttpService.unmarkRequest("getSalesTeam");

      	  		if (angular.isFunction(error)) {
      	  			error();
      	  		}
      		});
	}

	HttpService.sendTeamInvitation = function(invitationData, success, error){
		HttpService.markRequest("sendTeamInvitation");

      	$http.post($rootScope.server_url+"api/salesTeam/invite?key="+$rootScope.auth_key, invitationData)
	      	.success(function(response){
	        	HttpService.unmarkRequest("sendTeamInvitation");
	        	if (angular.isFunction(success)) {
	        		success(response);
	        	}
	      	})
	      	.error(function(response){
	      		ErrorService.catch(response);
	        	HttpService.unmarkRequest("sendTeamInvitation");
	        	if (angular.isFunction(error)) {
	        		error(response);
	        	}
	      	});
	}

	HttpService.getLevels = function(success, error){
		HttpService.markRequest("getLevels");

		$http.get($rootScope.server_url+"api/levels?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getLevels");
			  	$rootScope.levels = response.data;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getLevels");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});

	}

	HttpService.getGlobalRanking = function(success, error){
		HttpService.markRequest("getGlobalRanking");

		$http.get($rootScope.server_url+"api/ranking?key="+$rootScope.auth_key+"&length=20")
			.success(function(response){
			  	HttpService.unmarkRequest("getGlobalRanking");
			  	$rootScope.globalRanking = response.data;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getGlobalRanking");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getGoals = function(success, error){
		HttpService.markRequest("getGoals");

		$http.get($rootScope.server_url+"api/goals?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getGoals");
			  	$rootScope.goals = response.data;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getGoals");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getDiscountCodes = function(success, error){
		HttpService.markRequest("getDiscountCodes");

		$http.get($rootScope.server_url+"api/discountCode?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getDiscountCodes");
			  	$rootScope.discountCodes = response.data.discount_codes;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getDiscountCodes");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getMissions = function(success, error){
		HttpService.markRequest("getMissions");

		$http.get($rootScope.server_url+"api/missions?key="+$rootScope.auth_key)
			.success(function(response){
				HttpService.unmarkRequest("getMissions");

			  	$rootScope.missions = response.data.missions;
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getMissions");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.editMission = function(missionData, success, error){
		HttpService.markRequest("editMission");

		$http.post($rootScope.server_url+"api/adminMissions/edit?key="+$rootScope.auth_key, missionData)
			.success(function(response){
				HttpService.unmarkRequest("editMission");
				
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("editMission");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminMissions = function(missionsData, success, error){

		if (!angular.isObject(missionsData)) {
			missionsData = {};
			missionsData.limit = 20;
			missionsData.page  = 1;
		}

		$http.get($rootScope.server_url+"api/adminMissions/all/"+missionsData.limit+"/"+missionsData.page+"?key="+$rootScope.auth_key)
			.success(function(response){

				if (angular.isUndefined($rootScope.adminMissions)) {
					$rootScope.adminMissions = [];
				}

				if (missionsData.page > 1) {
					$rootScope.adminMissions = $rootScope.adminMissions.concat(response.data.missions);
				} else {
					$rootScope.adminMissions = response.data.missions;
				}

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.approveAdminMission = function(missionData, success, error){

		$http.post($rootScope.server_url+"api/adminMissions/approveMission?key="+$rootScope.auth_key, {
			id_user : missionData.idUser,
			id_mission : missionData.idMission
		})
			.success(function(response){

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.rejectAdminMission = function(missionData, success, error){

		$http.post($rootScope.server_url+"api/adminMissions/rejectMission?key="+$rootScope.auth_key, {
			id_user : missionData.idUser,
			id_mission : missionData.idMission,
			details : missionData.details,
		})
			.success(function(response){

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminGoals = function(goalsData, success, error){

		if (!angular.isObject(goalsData)) {
			goalsData = {};
			goalsData.limit = 20;
			goalsData.page  = 1;
		}

		$http.get($rootScope.server_url+"api/adminGoals/all/"+goalsData.limit+"/"+goalsData.page+"?key="+$rootScope.auth_key)
			.success(function(response){

				if (angular.isUndefined($rootScope.adminGoals)) {
					$rootScope.adminGoals = [];
				}

				if (goalsData.page > 1) {
					$rootScope.adminGoals = $rootScope.adminGoals.concat(response.data.goals);
				} else {
					$rootScope.adminGoals = response.data.goals;
				}

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.editGoal = function(goalData, success, error){
		HttpService.markRequest("editGoal");

		$http.post($rootScope.server_url+"api/adminGoals/edit?key="+$rootScope.auth_key, goalData)
			.success(function(response){
				HttpService.unmarkRequest("editGoal");
				
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("editGoal");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminGoalWinners = function(goalData, success, error){

		$http.post($rootScope.server_url+"api/adminGoals/viewWinners/"+goalData.limit+"/"+goalData.page+"?key="+$rootScope.auth_key, {
			id_goal : goalData.id_goal
		})
			.success(function(response){

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminStats = function(success, error){

		$http.get($rootScope.server_url+"api/adminStats?key="+$rootScope.auth_key)
			.success(function(response){

				$rootScope.adminStats = response.data.statistics;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminNotifications = function(notificationsData, success, error){

		if (!angular.isObject(notificationsData)) {
			notificationsData = {};
			notificationsData.limit = 20;
			notificationsData.page  = 1;
		}

		$http.get($rootScope.server_url+"api/adminNotifications/"+notificationsData.limit+"/"+notificationsData.page+"?key="+$rootScope.auth_key)
			.success(function(response){

				if (angular.isUndefined($rootScope.adminNotifications)) {
					$rootScope.adminNotifications = [];
				}

				if (notificationsData.page > 1) {
					$rootScope.adminNotifications = $rootScope.adminNotifications.concat(response.data.notifications);
				} else {
					$rootScope.adminNotifications = response.data.notifications;
				}

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminMissionUsers = function(missionData, success, error){

		$http.post($rootScope.server_url+"api/adminMissions/viewApplications/"+missionData.limit+"/"+missionData.page+"?key="+$rootScope.auth_key, {
			id_mission : missionData.id_mission
		})
			.success(function(response){

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getAdminMissionUserApplication = function(missionData, success, error){

		$http.post($rootScope.server_url+"api/adminMissions/viewApplication/?key="+$rootScope.auth_key, {
			id_mission : missionData.id_mission,
			id_user : missionData.id_user,
		})
			.success(function(response){

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response); 

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getCountries = function(success, error){
		HttpService.markRequest("getCountries");

		$http.get($rootScope.server_url+"api/countries?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getCountries");
			  	$rootScope.countries = response.data.countries;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getCountries");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getStates = function(success, error){
		HttpService.markRequest("getStates");

		$http.get($rootScope.server_url+"api/states?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getStates");
			  	$rootScope.states = response.data.states;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getStates");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getCities = function(citiesData, success, error){
		HttpService.markRequest("getCities");

		$http.get($rootScope.server_url+"api/cities?key="+$rootScope.auth_key+"&country="+citiesData.country+"&state="+citiesData.state)
			.success(function(response){
			  	HttpService.unmarkRequest("getCities");
			  	$rootScope.cities = response.data.cities;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getCities");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getSchools = function(schoolsData, success, error){
		HttpService.markRequest("getSchools");

		$http.get($rootScope.server_url+"api/schools?key="+$rootScope.auth_key+"&id_country="+schoolsData.country+"&state="+schoolsData.state+"&city="+schoolsData.city)
			.success(function(response){
			  	HttpService.unmarkRequest("getSchools");
			  	$rootScope.schools = response.data.schools;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getSchools");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getBlogs = function(latest, success, error){
		HttpService.markRequest("getBlogs");

		$http.get($rootScope.server_url+"api/blogs/latest/"+latest+"?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getBlogs");
			  	$rootScope.blogs = response.data.blogs;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getBlogs");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getPayouts = function(success, error){
		HttpService.markRequest("getPayouts");

		$http.get($rootScope.server_url+"api/payouts?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getPayouts");
			  	$rootScope.payouts = response.data.payouts;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getPayouts");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.getTerms = function(){
		HttpService.markRequest("getTerms");

		$http.get($rootScope.server_url+"api/terms?key="+$rootScope.auth_key)
			.success(function(response){
			  	HttpService.unmarkRequest("getTerms");
			  	$rootScope.terms = response.data.terms;

			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("getTerms");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.requestPayment = function(paymentData, success, error){
		HttpService.markRequest("requestPayment");

		$http.post($rootScope.server_url+"api/payouts/requestPayment?key="+$rootScope.auth_key, paymentData)
			.success(function(response){
			  	HttpService.unmarkRequest("requestPayment");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("requestPayment");
				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}

	HttpService.updateProfile = function(profileData, success, error){
		HttpService.markRequest("updateProfile");

		$http.post($rootScope.server_url+"api/profile/edit?key="+$rootScope.auth_key, profileData)
			.success(function(response){
			  	HttpService.unmarkRequest("updateProfile");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
				HttpService.unmarkRequest("updateProfile");
				if (angular.isFunction(error)) {
					error(response);
				}
			});
	}


	HttpService.postCode = function(codeData, success, error){
		HttpService.markRequest("postCode");

		$http.post($rootScope.server_url+"api/discountCode/create?key="+$rootScope.auth_key, codeData)
			.success(function(response){
			  	HttpService.unmarkRequest("postCode");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("postCode");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.postMission = function(id_mission, missionData, success, error){
		// HttpService.markRequest("postMission");
		$http.post($rootScope.server_url+"api/missions/"+id_mission+"?key="+$rootScope.auth_key, missionData)
			.success(function(response){
			  	HttpService.unmarkRequest("postMission");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("postMission");
			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.postSharedMission = function(id_mission, network, success, error){
		HttpService.markRequest("postSharedMission");

		$http.post($rootScope.server_url+"api/missions/"+id_mission+"/share_completed?key="+$rootScope.auth_key, {
			network : network
		})
			.success(function(response){
				HttpService.unmarkRequest("postSharedMission");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("postSharedMission");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.forgotPassword = function(email, success, error){
		HttpService.markRequest("forgotPassword");

		$http.post($rootScope.server_url+"api/forgotPassword?key="+$rootScope.auth_key, {
			email : email
		})
			.success(function(response){
				HttpService.unmarkRequest("forgotPassword");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("forgotPassword");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.resetPassword = function(resetData, success, error){
		HttpService.markRequest("resetPassword");

		$http.post($rootScope.server_url+"api/resetPassword?key="+$rootScope.auth_key, resetData)
			.success(function(response){
				HttpService.unmarkRequest("resetPassword");
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(response);
			  	HttpService.unmarkRequest("resetPassword");

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	HttpService.setInvitation = function(invitationData, success, error){

		$http.post($rootScope.server_url+"api/setInvitation?key="+$rootScope.auth_key, invitationData)
			.success(function(response){
			  	if (angular.isFunction(success)) {
			  		success(response);
			  	}
			})
			.error(function(response){
				ErrorService.catch(responsfe);

			  	if (angular.isFunction(error)) {
			  		error(response);
			  	}
			});
	}

	return HttpService;
});
