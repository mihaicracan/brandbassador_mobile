
angular.module('starter')

.factory('StringsService', function($window, $rootScope, $location) {
	var StringsService = {};

	StringsService.getStrings = function(page){
		if (angular.isDefined(page) && page.length > 0 && angular.isDefined(this.strings[page])) {
			return this.strings[page];
		}

		return this.strings;
	}

	StringsService.strings = {
		// Months
		months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

		// Menu Section
		menu: {
			itemDashboard: "Dashboard",
			itemProfile: "Profile",
			itemCodes: "Discount Codes",
			itemTeam: "Sales Team",
			itemRanking: "Ranking",
			itemLevels: "Levels",
			itemGoals: "Goals",
			itemMissions: "Missions",
			itemPayouts: "Payouts",
			itemBrandbassadors: "Brandbassadors",
			itemSettings: "Settings",
			itemLogout: "Logout"
		},

		admin: {
			brandbassadors: {
				pageTitle: "Brandbassadors",
				pageInfo: "Here you can view and approve brandbassadors.",

				alertTitleApproveCompleted: "Approve Brandbassador",
				alertApproveCompleted: "Brandbassador has been approved.",

				confirmTitleApprove: "Approve Brandbassador",
				confirmApprove: "Are you sure you want to approve this Brandbassador?",

				topTableTitle: "Brandbassadors",
				topTableHeaderName: "Name",
				topTableHeaderStatus: "Status",
				topTableHeaderFollowers: "Followers",
				tableNoData: "There is no Brandbassador here, yet.",

				labelStatusActive: "Approved",
				labelLocationNotProvided: "Unkown",

				actionApprove: "Approve",
			},

			brandbassadorView: {
				pageInfo: "View brandbassador details.",

				labelGlobal: "Global Rank",
				labelCountry: "Country Rank",
				labelCity: "City Rank",
				labelSalesPoints: "Sales Points",
				labelBonusPoints: "Bonus Points",
				labelSales: "Total Sales",
				labelCodes: "Discount Codes",
				labelEarnings: "Total Earnings",
				labelCommission: "Commission",
			},

			dashboard: {
				pageTitle: "Dashboard",

				labelUsers: "Brandbassadors",
				labelPendingUsers: "Pending Brandbassadors",
				labelSales: "Total Sales",
				labelFollowers: "Total Followers",
				labelFollowersLastDay: "Followers Last 24h",
				labelSalesPoints: "Sales Points",
				labelBonusPoints: "Bonus Points",
				labelEarnings: "Total Earnings",
				labelCommission: "Total Commission",
				labelNotifications: "Recent Notifications",
				labelNoNotifications: "No notification right now."
			},

			missionUsers: {
				pageInfo: "Brandbassadors that applied for this mission.",

				topTableTitle: "Brandbassadors",
				topTableHeaderName: "Name",
				tableNoData: "No application right now.",

				actionEdit: "Edit Mission"

			},

			missionUserApplication: {
				pageInfo: "Mission application details.",

				confirmTitleApprove: "Approve Mission",
				confirmTitleReject: "Reject Mission",
				confirmApprove: "Are you sure you want to approve this mission?",
				confirmReject: "Why do you want to reject this mission?",

				alertTitleApproveCompleted: "Mission Approved",
				alertTitleRejectCompleted: "Mission Rejected",
				alertApproveCompleted: "This mission has been approved",
				alertRejectCompleted: "This mission has been rejected",

				actionApprove: "Approve",
				actionReject: "Reject",

				labelName: "Name",
				labelSubmitted: "Submitted",
				labelStatus: "Status",
				labelStatusApproved: "Approved",
				labelStatusPending: "Pending",
				labelStatusRejected: "Rejected",
				labelLink: "Link",
				labelText: "Text",
				labelImage: "Image",
				labelFacebook: "Facebook",
				labelTwitter: "Twitter",
				labelPinterest: "Pinterest",
				labelNotProvided: "Not Provided",
				labelShared: "Shared",
				labelNotShared: "Not Shared",
			},

			missionEdit: {
				pageInfo: "Edit mission details.",

				confirmTitleEdit: "Mission Details",
				confirmEdit: "Are you sure you want to save this changes?",

				labelGeneral: "General",
				labelResponse: "Response",
				labelShare: "Share",
				labelFacebookShare: "Facebook Share",
				labelTwitterShare: "Twitter Share",
				labelPinterestShare: "Pinterest Share",

				inputInfoFeatured: "Featured",
				inputInfoName: "Mission Name",
				inputInfoCountdown: "Show Countdown",
				inputInfoText: "Allow Text",
				inputInfoLink: "Allow Link",
				inputInfoImage: "Allow Image",
				inputInfoLinkDetails: "Link Details",
				inputInfoTextDetails: "Text Details",
				inputInfoFBShare: "Facebook Share",
				inputInfoFBText: "Text to share",
				inputInfoFBLink: "Link to share",
				inputInfoTWShare: "Twitter Share",
				inputInfoTWText: "Text to share",
				inputInfoTWLink: "Link to share",
				inputInfoPINShare: "Pinterest Share",
				inputInfoPINText: "Text to share",
				inputInfoPINLink: "Link to share",

				actionSave: "Save"
			},

			goalWinners: {
				pageInfo: "Brandbassadors that are winning this goal.",

				topTableTitle: "Winners",
				topTableHeaderName: "Name",
				tableNoData: "No winners for now.",

				actionEdit: "Edit Goal"
			},

			goalEdit: {
				pageInfo: "Edit goal details.",

				confirmTitleEdit: "Goal Details",
				confirmEdit: "Are you sure you want to save this changes?",

				inputInfoTitle: "Title",
				inputInfoInvited: "Invited",
				inputInfoPoints: "Points",
				inputInfoAge: "Required Age",

				actionSave: "Save"
			},

			goals: {
				pageTitle: "Goals",
				pageInfo: "Here you can view and edit goals.",

				topTableTitle: "Goals",
				topTableHeaderName: "Name",
				tableNoData: "Start now and add some goals.",
			},

			missions: {
				pageTitle: "Missions",
				pageInfo: "Here you can view and edit missions.",

				topTableTitle: "Missions",
				topTableHeaderName: "Name",
				tableNoData: "Start now and create some missions."
			},

			settings: {
				pageTitle: "Settings",
				pageInfo: "Edit company settings.",

				confirmTitleSettings: "Settings",
				confirmSettings: "Are you sure you want to save these settings?",

				alertTitleSettings: "Settings",
				alertSettings: "Current settings have been saved.",

				labelCompany: "Company Details",
				labelRegistrationUrl: "Registration URL",
				labelColors: "Colors",
				labelCommission: "Commission",
				labelBestPerformers: "Best Performers",
				labelLevels: "Levels",
				labelCodes: "Discount Codes",
				labelPayouts: "Payouts",

				inputInfoCompanyName: "Company Name",
				inputInfoCompanyPhone: "Company Phone",
				inputInfoCompanyAddress1: "Company Address1",
				inputInfoCompanyAddress2: "Company Address2",
				inputInfoRegistrationUrl: "Registration Url",
				inputInfoPrimaryColor: "Primary Color",
				inputInfoSecondaryColor: "Secondary Color",
				inputInfoSingleUseLimit: "SingleUse Codes per day",
				inputInfoMultiUseLimit: "MultiUse codes per week",
				inputInfoLevel1Commission: "Commission Percentage for Level 1",
				inputInfoLevel2Commission: "Commission Percentage for Level 2",
				inputInfoLevel3Commission: "Commission Percentage for Level 3",
				inputInfoSlaveCommission: "Commission Percentage for UnderKeeper",
				inputInfoMinPayout: "Minimum Payout",
				inputInfoLimitPayout: "Payout Requests per Month",
				inputInfoBonusPointsLevel2: "Bonus Points on Level 2",
				inputInfoBonusPointsLevel3: "Bonus Points on Level 3",
				inputInfoTotalPointsLevel2: "Total Points for Level 2",
				inputInfoTotalPointsLevel3: "Total Points for Level 3",
				inputInfoSalesLevel2: "Sales for Level 2",
				inputInfoSalesLevel3: "Sales for Level 3",
				inputInfoBestPerformersDays: "Days Interval for Best Performers",
				inputInfoBestPerformersSize: "Best Performers List Size",
				inputInfoBestPerformersPoints: "Best Performers Points",

				actionSave: "Save"
			}
		},

		// Dashboard Page
		dashboard: {
			pageTitle: "Dashboard",

			labelCreateCode: "Create a promo code",
			labelEarningsCash: "Earnings Cash",
			labelEarningsGiftcard: "Earnings Giftcard",
			labelGlobalRanking: "Global Ranking",
			labelTotalPoints: "Total Points",
			labelBonusPoints: "Bonus",
			labelSalesPoints: "Sales",
			labelLevel: "Level",
			labelSales: "Sales",
			labelBlog: "Blog",
			labelLatestPosts: "Latest Posts"
		},

		// Codes Page
		codes: {
			pageInfo: "You can create discount codes to send to a friend or use them on social media networks.",
			pageTitle: "Discount Codes",

			alertTitle: "Discount Code",

			inputCodeInfo: "Your Code (letters and numbers)",
			inputLinkInfo: "Link to where you will post you code",

			labelMultiuseType: "Multi-use",
			labelSingleuseType: "Single-use",
			labelCreatedSuccess: "Your code has been created.",
			labelShare: "Share this discount code on your social networks.",
			labelRequestTitle: "Discount Code",
			labelStatusPending: "Pending",
			labelShareDescription: "I have just created the following code:",
			labelFBShare: "Share on Facebook",
			labelTWShare: "Share on Twitter",

			actionRequest: "Create",

			codesTableTitle: "Discount Codes",
			codesTableHeaderCode: "Code",
			codesTableHeaderDiscount: "Discount",
			codesTableHeaderExpires: "Expires",
			codesTableNoData: "Start now and create some discount codes!"
		},

		// Goals Page
		goals: {
			pageTitle: "Goals",
			pageInfo: "Goals will be based on Brandbassadors reaching a certain number of points.",

			labelPoints: "Points required:",
			labelInvited: "Who will be invited:",
			labelDeadline: "Deadline:",
			labelTop: "Top",
			labelPieceKeepers: "Brandbassadors",
			labelStatusExpired: "Expired",
			labelStatusTooYoung: "Too young",
			labelStatusAccepted: "You're in",
			labelStatusLowRank: "Ranking too low",
			labelRankGlobal: "Your Global Rank",
			labelRankCountry: "Your Country Rank",
			labelRankCity: "Your City Rank",

			tableNoData: "There are no goals yet."
		},

		// Level Page
		levels: {
			pageTitle: "Levels",

			labelLevel1: "Level 1",
			labelLevel2: "Level 2",
			labelLevel3: "Level 3",
			labelCurrentLevel: "your current level",
			labelCommission: "commission on all sales"
		},

		// Forgot Password Page
		forgot: {
			alertTitle: "Recover Password",
			alertSentSuccess: "You will receive an email notification.",

			inputEmailInfo: "Email Address",

			actionSend: "Recover",
		},

		// Login Page
		login: {
			alertTitle: "Login",
			alertTitleFB: "Facebook Login",

			labelForgotPassword: "Forgot Password?",
			labelRegister: "Register",

			inputEmailInfo: "Email Address",
			inputPasswordInfo: "Password",

			actionLogin: "Log In",
			actionFBLogin: "Log In with Facebook"
		},

		// Missions Page
		missions: {
			pageTitle: "Missions",

			alertTitleSubmitted: "Mission Submitted",
			alertTitleUnsubmitted: "Mission Not Submitted",
			alertTitleShared: "Share Completed",
			alertSubmittedSuccess: "Please wait for admin approval.",
			alertSharedFBSuccess: "You've successfully shared this mission on Facebook.",
			alertSharedTWSuccess: "You've successfully shared this mission on Twitter.",

			labelPoints: "Points",
			labelFBShare: "Share on Facebook",
			labelTWShare: "Share on Twitter",
			labelPINShare: "Share on Pinterest",
			labelUploadPhoto: "Upload Photo",
			labelIncompleted: "Incompleted",
			labelWaiting: "Waiting",
			labelCompleted: "Completed",

			tableNoData: "There are no missions yet.",

			actionSend: "Send"
		},

		// Payouts Page
		payouts: {
			pageTitle: "Payouts",
			pageInfo: "There are restrictions on how much you can earn as a Brandbassadors and this varies from country to country. Check <a href='#/terms'>Terms and Conditions</a> for more information.",

			labelStatusRejected: "Rejected",
			labelStatusWaiting: "Waiting",
			labelStatusApproved: "Approved",
			labelCommission: "Your commission:",
			labelGiftcardPre: "as",
			labelGiftcard: "Giftcard",
			labelGiftcardExtra: "(Get 200% Extra)",
			labelPayPalPre: "to",
			labelPayPal: "Pay to PayPal account",

			alertTitleRequest: "Request Payment",
			alertTitlePayPal: "PayPal",
			alertRequestSuccessGiftcard: "Your giftcard will be sent by email.",
			alertRequestSuccessPayPal: "Please wait for admin approval.",
			alertPayPalSuccess: "Your PayPal email address has been updated.",

			confirmTitlePayment: "Request Payment",
			confirmTitlePayPal: "PayPal",
			confirmPaymentGiftcard: "Are you sure you want this payment as a Giftcard?",
			confirmPaymentPayPal: "Are you sure you want this payment to your PayPal account?",
			confirmPayPal: "Are you sure you want to change your PayPal email address?",

			inputPayPalInfo: "Your PayPal Email",
			inputInfoAmount: "Amount",

			actionUpdatePayPal: "Update PayPal Email",
			actionRequest: "Request Payment",

			paymentsTableTitle: "Payments",
			paymentsTableHeaderAmount: "Amount",
			paymentsTableHeaderStatus: "Status",
			paymentsTableHeaderDate: "Date",
			paymentsTableNoData: "You don't have any payments yet.",

			giftcardsTableTitle: "Giftcards",
			giftcardsTableHeaderCode: "Code",
			giftcardsTableHeaderDiscount: "Discount",
			giftcardsTableNoData: "You don't have any giftcards yet."
		},

		// Profile Page
		profile: {
			pageTitle: "Profile",
			pageInfo: "View and edit information about your profile and social networks connections.",

			confirmTitleUpdate: "Update Profile",
			confirmUpdate: "Are you sure you want to update your profile?",

			alertTitleUpdate: "Update Profile",
			alertUpdateSuccess: "Your profile has been updated.",

			inputFirstNameInfo: "First Name",
			inputLastNameInfo: "Last Name",
			inputEmailInfo: "Your Email",
			inputGenderInfo: "Gender",
			inputPhoneInfo: "Phone",
			inputBirthdayInfo: "Birthday",
			inputAddress1Info: "Address 1",
			inputAddress2Info: "Address 2",
			inputCodeInfo: "Zip/Postal Code",
			inputCityInfo: "City",
			inputSchoolInfo: "School",
			inputGraduationInfo: "Graduation Year",

			labelMale: "Male",
			labelFemale: "Female",
			labelDay: "Day",
			labelMonth: "Month",
			labelYear: "Year",
			labelAddress: "Address",
			labelCountry: "Country",
			labelState: "State",
			labelCity: "City",
			labelSchool: "School",
			labelOther: "Other",

			actionUpdate: "Update Profile",
			actionEditNetworks: "Edit Social Networks"
		},

		// Ranking Page
		ranking: {
			pageTitle: "Ranking",
			pageInfo: "Your ranking shows how you are doing compared to other Brandbassadors.",

			statusLastMonth: "Last Month",
			statusSameLastMonth: "Same as Last Month",

			labelGlobal: "Global",
			labelCountry: "Country",
			labelCity: "City",

			topTableTitle: "Top 20 Global Brandbassadors",
			topTableHeaderName: "Name",
			topTableHeaderCountry: "Country",
			topTableHeaderPoints: "Total Pts"

		},

		// Register Page
		register: {
			pageInfo: "Please register with your Facebook account.",

			alertTitleFB: "Facebook Login",
			alertTitleRegister: "Register",
			alertTitleImage: "Upload Image",
			alertFBError: "Something went wrong.",
			alertRegisterErrorPassword: "Please choose a password.",
			alertRegisterErrorPasswordConfirm: "Password confirmation doesn't match your password.",
			alertRegisterErrorPhotoSize: "Your photo has reached the maximum size of 2MB.",
			alertImageFBUploaded: "Your Facebook profile image is already uploaded.",

			inputEmailInfo: "Email",
			inputPasswordInfo: "Password",
			inputConfirmPasswordInfo: "Confirm Password",
			inputFirstNameInfo: "First Name",
			inputLastNameInfo: "Last Name",
			inputGenderInfo: "Gender",
			inputPhoneInfo: "Phone",
			inputBirthdayInfo: "Birthday",
			inputAddress1Info: "Address line 1",
			inputAddress2Info: "Address line 2",
			inputCodeInfo: "Zip/Postal Code",
			inputCityInfo: "City",
			inputSchoolInfo: "School",
			inputGraduationInfo: "Graduation Year",

			labelPhotoSelectorTitle: "Choose Photo",
			labelPhotoSelectorTitleFB: "Facebook Profile Image",
			labelRegisterFB: "Register with Facebook",
			labelImages: "Please upload 2 profile images",
			labelMale: "Male",
			labelFemale: "Female",
			labelDay: "Day",
			labelMonth: "Month",
			labelYear: "Year",
			labelAddress: "Address",
			labelCountry: "Country",
			labelState: "State",
			labelCity: "City",
			labelSchool: "School",
			labelOther: "Other",
			labelAccept: "I accept",
			labelTerms: "terms and conditions",

			actionSubmit: "Next"
		},

		// Reset Password Page
		reset: {
			alertTitleReset: "Reset Password",
			alertTitleResetSuccess: "Reset Completed",
			alertResetSuccess: "You can now login to your account.",

			inputNewPasswordInfo: "New Password",
			inputConfirmPasswordInfo: "Confirm Password",

			actionReset: "Reset"
		},

		// Sales Team Page
		salesteam: {
			pageTitle: "Sales Team",
			pageInfo: "Since you have reached level 3 of the Brandbassador program you have proven to us that you are a great ambassador. Now you have the chance to create a sales team to work for you.",

			labelStatusAccepted: "Accepted",
			labelStatusPending: "Pending",
			labelInvite: "You can invite",
			labelUnderKeepers: "UnderKeepers",
			labelInvitations: "invitations left",

			inputUnderKeeperInfo: "UnderKeeper Email",

			alertTitleInvite: "Invite UnderKeeper",
			alertInviteSuccess: "Your invitation has been sent.",

			confirmTitleInvite: "Invite UnderKeeper",
			confirmInvite: "Are your sure you want to send this invitation?",

			underKeepersTableTitle: "UnderKeepers",
			underKeepersTableHeaderEmail: "Email",
			underKeepersTableHeaderStatus: "Status",
			underKeepersTableNoData: "Start now and build your team!",

			actionInvite: "Invite"
		},

		// Social Linking Page
		social: {
			pageTitleProfile: "Social Networks",
			pageInfoProfile: "Change and update your social networks accounts.",
			pageInfo: "Please link up your social media profile below. We <b>ONLY</b> pull your following count so we can determine your social reach. The more accounts you enter the bigger chance of being accepted. <br/><br/> <b>You have to enter at least one social account.</b>",
			
			alertTitleChooseFBAccount: "Choose Account",
			alertTitleRegisterSuccess: "Register Completed",
			alertTitleRegister: "Register",
			alertTitleLink: "Link Social Network",
			alertTitleFB: "Facebook Linking",
			alertTitleTW: "Twitter Linking",
			alertTitleINST: "Instagram Linking",
			alertTitleTUB: "Tumblr Linking",
			alertTitlePIN: "Pinterest Linking",
			alertTitleYB: "Youtube Linking",
			alertTitleVN: "Vine Linking",
			alertRegisterSuccess: "Please wait for admin approval.",

			popupTitlePIN: "Enter your Pinterest username",
			popupTitleVN:  "Enter your Vine user ID",

			actionLink: "Link",
			actionFBLink: "Link Facebook",
			actionTWLink: "Link Twitter",
			actionINSTLink: "Link Instagram",
			actionTUBLink: "Link Tumblr",
			actionPINLink: "Link Pinterest",
			actionYBLink: "Link Youtube",
			actionVNLink: "Link Vine",
			actionSubmit: "Submit"
		},

		terms: {
			pageTitle: "Terms and Conditions"
		},

		errors: {
			alertTitleError: "Something went wrong",

			// General
			requiredEmail: "You have to enter an email address.",
			invalidEmail: "You have to enter a valid email address.",
			facebookError: "Facebook cannot connect to your account.",
			duplicateEmail: "This email already exists.",

			// Discount codes
			invalidCouponType: "Invalid coupon type.",
			companyAlreadyHasThisCode: "This discount code has already been created for this company.",
			errorWhileCreatingCode: "Your discount code has not been created. Choose another code and try again.",
			invalidUserLevel: "You can't create multiuse discount codes right now.",
			invalidCouponCode: "You have to enter a valid discount code.",
			invalidCouponLink: "You have to enter the link where you'll post the code.",

			// Login
			invalidLoginCredentials: "Invalid email or password.",
			maximumLoginAttemptsExceeded: "Your IP address has been blocked for 24 hours.",
			invalidFacebookAccount: "There is no user associated with your Facebook account. Please register.",
			notAllowed: "Invalid email or password.",

			// Missions
			requiredText: "You have to provide your text response.",
			requiredLink: "You have to provide your link response.",

			// Payouts
			invalidPaymentMethod: "Please choose a payment method.",
			requiredPayPalEmail: "Please set your PayPal email.",
			maximumLimitOfRequestsReached: "You have reached monthly requests limit.",
			invalidAmountValue: "You need at least $10 before you can ask for payout.",
			notEnoughCredit: "You don't have enough credit for this payout.",
			requiredAmount: "You have to choose your payout amount.",
			invalidPayPalEmail: "You have to enter a valid PayPal email address.",

			// Reset
			requiredNewPassword: "Please choose a new password.",
			invalidPasswordConfirm: "Password confirmation doesn't match New Password.",

			// Profile
			requiredFirstName: "First Name is required.",
			requiredLastName: "Last Name is required.",
			requiredDayBirthday: "Birthday is required.",
			requiredMonthBirthday: "Birthday is required.",
			requiredYearBirthday: "Birthday is required.",
			requiredAddress: "Address is required.",
			requiredCountry: "Country is required.",
			requiredCity: "City is required.",
			requiredPhone: "Phone is required.",
			requiredState: "State is required.",

			// Sales Team
			invalidMasterLevel: "You don't have permissions for this kind of request.",
			slavesLimitReached: "You have reached your limit for invitations.",

			// FB Register
			deniedFBEmail: "You have to provide us access to your Facebook email address.",
			userAlreadyExists: "This email already exists in our system.",

			// Social
			socialAccountAlreadyLinked: "This social account has already been linked.",
			invalidPINUsername: "This Pinterest username is invalid.",
			requiredPINUsername: "Pinterest username is required.",
			invalidVNUserID: "This Vine user ID is invalid.",
			requiredVNUserID: "Vine user ID is required.",
			invalidOAuthURL: "We couldn't link this Social Network. Please try again.",

			noSocialNetwork: "You need to link at least one social network.",

		}
	}

	return StringsService;
});
