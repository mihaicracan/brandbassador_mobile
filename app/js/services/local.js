
angular.module('starter')

.factory('LocalService', function($rootScope) {
	var LocalService = {};

	LocalService.get = function(data, callback){
		if (angular.isDefined($rootScope[data])) {
			if (angular.isFunction(callback)) {
				callback($rootScope[data]);
			}
		} else {
			$rootScope.$watch(data, function(value){
		    	if (angular.isFunction(callback) && angular.isDefined(value)) {
		    		callback(value);
		    	}
		    });
		}
	}

	return LocalService;
});
