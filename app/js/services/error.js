
angular.module('starter')

.factory('ErrorService', function($window, $rootScope, $location, $ionicPopup) {
	var ErrorService = {};

	ErrorService.title     = "";
	ErrorService.messages  = [];
	ErrorService.errors    = [];

	ErrorService.init = function(data){
		this.errors = [];
		if (angular.isObject(data)) {
			if (angular.isDefined(data.errors)) {
				this.errors = data.errors;
			} else {
				this.errors = data;
			}
		} else if (angular.isString(data)) {
			this.errors.push(data);
		}

		this.title = $rootScope.strings.errors.alertTitleError;
		this.messages = [];
	}

	ErrorService.catch = function(data, title){
		this.init(data);

		// parse errors and add messages
		for (var i in this.errors) {
			var error = this.errors[i];

			this.addMessage(this.toMessage(error));
		}

		this.display();
	}

	ErrorService.toMessage = function(error) {
		if (angular.isDefined($rootScope.strings.errors[error])) {
			return $rootScope.strings.errors[error];
		}

		console.error(error+" message not defined");
		return null;
	}

	ErrorService.addMessage = function(message) {
		if (angular.isString(message) && message.length > 0) {
			this.messages.push(message);
		}
	}

	ErrorService.display = function(){
		var template = "";

		if (this.messages.length == 0) {
			return;
		}

		for (var i in this.messages) {
			template += "<p>" + this.messages[i] + "</p>";
		}

		$ionicPopup.alert({
		    title: this.title,
		    template: template
		});
	}

	return ErrorService;
});
