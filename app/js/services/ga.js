
angular.module('starter')

.factory('GAService', function($window, $rootScope, $location) {
	var GAService = {};

	GAService.enableTracking = function(){
		if ($location.host().match(/brandbassador.com/)) {
			return true;
		}

		return false;
	}

	GAService.trackPage = function(location){
		if (this.enableTracking()) {
			$window._gaq.push(['_trackPageview', location]);
		}
	}

	GAService.trackEvent = function(category, action, label, value){
		if (this.enableTracking()) {
			$window._gaq.push(['_trackEvent', category, action, label, value]);
		}
	}

	GAService.trackRequestTime = function(action, label, value){
		this.trackEvent("Request", action, null, value);
	}

	return GAService;
});
